/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2022 Mark Roszko <mark.roszko@gmail.com>
 * Copyright (C) 1992-2022 KiCad Developers, see AUTHORS.txt for contributors.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMAND_SYM_H
#define COMMAND_SYM_H

#include "command.h"

// 定义了一个名为CLI的命名空间，其中包含一个名为SYM_COMMAND的结构体，
namespace CLI
{
struct SYM_COMMAND : public COMMAND
{
    // SYM_COMMAND结构体使用了m_argParser.add_description()方法，该方法用于向命令行解析器添加命令的描述信息。
    SYM_COMMAND() : COMMAND( "sym" ) {
        m_argParser.add_description( UTF8STDSTR( _( "Symbol and Symbol Libraries" ) ) );
    }
};
}

#endif