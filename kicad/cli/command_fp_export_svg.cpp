/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2022 Mark Roszko <mark.roszko@gmail.com>
 * Copyright (C) 1992-2023 KiCad Developers, see AUTHORS.txt for contributors.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "command_fp_export_svg.h"
#include <cli/exit_codes.h>
#include "jobs/job_fp_export_svg.h"
#include <kiface_base.h>
#include <layer_ids.h>
#include <string_utils.h>
#include <wx/crt.h>
#include <wx/dir.h>

#include <macros.h>
#include <wx/tokenzr.h>

#define ARG_FOOTPRINT "--footprint"

// FP_EXPORT_SVG_COMMAND类的构造函数中，调用了 PCB_EXPORT_BASE_COMMAND 类的构造函数，并传递了三个参数：
CLI::FP_EXPORT_SVG_COMMAND::FP_EXPORT_SVG_COMMAND() : PCB_EXPORT_BASE_COMMAND( "svg", true, true )
{
    // 参数解析器添加了一个描述
    m_argParser.add_description( UTF8STDSTR( _( "Exports the footprint or entire footprint "
                                                "library to SVG" ) ) );

    addLayerArg( false );
    addDefineArg();
    // 向参数解析器添加了一个名为"-t"的参数，并使用ARG_THEME作为其唯一标识符。
    // .default_value( std::string() )设置了参数的默认值为空字符串，.help()提供了参数的帮助信息。
    m_argParser.add_argument( "-t", ARG_THEME )
            .default_value( std::string() )
            .help( UTF8STDSTR( _( "Color theme to use (will default to footprint editor "
                                  "settings)" ) ) );
    
    // 添加了一个名为"--fp"的参数，并使用ARG_FOOTPRINT作为其唯一标识符。
    m_argParser.add_argument( "--fp", ARG_FOOTPRINT )
            .default_value( std::string() )
            .help( UTF8STDSTR( _( "Specific footprint to export within the library" ) ) )
            .metavar( "FOOTPRINT_NAME" );
    
    // 添加了一个名为ARG_BLACKANDWHITE的参数
    m_argParser.add_argument( ARG_BLACKANDWHITE )
            .help( UTF8STDSTR( _( ARG_BLACKANDWHITE_DESC ) ) )
            .flag();
}

// 导出SVG命令的实际逻辑，包括设置导出参数、检查输入的有效性、执行导出作业等。
int CLI::FP_EXPORT_SVG_COMMAND::doPerform( KIWAY& aKiway )
{
    // 调用了基类 PCB_EXPORT_BASE_COMMAND 的 doPerform 方法，并将结果存储在 baseExit 变量中。
    int baseExit = PCB_EXPORT_BASE_COMMAND::doPerform( aKiway );
    if( baseExit != EXIT_CODES::OK )
        return baseExit;

    std::unique_ptr<JOB_FP_EXPORT_SVG> svgJob = std::make_unique<JOB_FP_EXPORT_SVG>( true );
    // 设置了 svgJob 对象的一些属性，导出的封装库的路径。文件的输出目录。图形是否为黑白模式。具体封装的名称。
    svgJob->m_libraryPath = m_argInput;
    svgJob->m_outputDirectory = m_argOutput;
    svgJob->m_blackAndWhite = m_argParser.get<bool>( ARG_BLACKANDWHITE );
    svgJob->m_footprint = From_UTF8( m_argParser.get<std::string>( ARG_FOOTPRINT ).c_str() );
    svgJob->SetVarOverrides( m_argDefineVars );

    // 检查了要导出的封装库是否存在。
    if( !wxDir::Exists( svgJob->m_libraryPath ) )
    {
        wxFprintf( stderr, _( "Footprint library does not exist or is not accessible\n" ) );
        return EXIT_CODES::ERR_INVALID_INPUT_FILE;
    }

    // 设置了颜色主题。
    svgJob->m_colorTheme = From_UTF8( m_argParser.get<std::string>( ARG_THEME ).c_str() );

    // 设置了要导出的图层。
    if( !m_selectedLayers.empty() )
        svgJob->m_printMaskLayer = m_selectedLayers;
    else
        svgJob->m_printMaskLayer = LSET::AllLayersMask().SeqStackupForPlotting();

    int exitCode = aKiway.ProcessJob( KIWAY::FACE_PCB, svgJob.get() );

    return exitCode;
}
