/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2022 Mark Roszko <mark.roszko@gmail.com>
 * Copyright (C) 1992-2023 KiCad Developers, see AUTHORS.txt for contributors.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "command_fp_upgrade.h"
#include <cli/exit_codes.h>
#include "jobs/job_fp_upgrade.h"
#include <kiface_base.h>
#include <layer_ids.h>
#include <wx/crt.h>
#include <wx/dir.h>

#include <macros.h>

#define ARG_FORCE "--force"

//  CLI::FP_UPGRADE_COMMAND::FP_UPGRADE_COMMAND() 是该类的构造函数。
// 它在对象被创建时被调用，用于初始化对象的成员变量和执行其他必要的设置。
CLI::FP_UPGRADE_COMMAND::FP_UPGRADE_COMMAND() : PCB_EXPORT_BASE_COMMAND( "upgrade", true, true )
{
    // 添加了命令的描述信息。
    m_argParser.add_description( UTF8STDSTR( _( "Upgrades the footprint library to the current "
                                                "kicad version format" ) ) );
    // 添加了一个名为 ARG_FORCE 的命令行参数。
    m_argParser.add_argument( ARG_FORCE )
            .help( UTF8STDSTR(
                    _( "Forces the footprint library to be resaved regardless of versioning" ) ) )
            .flag();
}


int CLI::FP_UPGRADE_COMMAND::doPerform( KIWAY& aKiway )
{
    // 通过 std::make_unique<JOB_FP_UPGRADE>(true) 创建了一个名为 fpJob 的 JOB_FP_UPGRADE 对象。
    std::unique_ptr<JOB_FP_UPGRADE> fpJob = std::make_unique<JOB_FP_UPGRADE>( true );

    // 将命令中的输入路径（m_argInput）赋值给 fpJob 对象的 m_libraryPath 属性。
    fpJob->m_libraryPath = m_argInput;
    // 将命令中的输出路径（m_argOutput）赋值给 fpJob 对象的 m_outputLibraryPath 属性。
    fpJob->m_outputLibraryPath = m_argOutput;
    // 将命令中的强制标志（ARG_FORCE）赋值给 fpJob 对象的 m_force 属性。
    fpJob->m_force = m_argParser.get<bool>( ARG_FORCE );

    // 调用 aKiway 对象的 ProcessJob 方法，传递 FACE_PCB 参数和 fpJob 指针的值作为参数
    int exitCode = aKiway.ProcessJob( KIWAY::FACE_PCB, fpJob.get() );

    return exitCode;
}
