/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2022 Mark Roszko <mark.roszko@gmail.com>
 * Copyright (C) 1992-2023 KiCad Developers, see AUTHORS.txt for contributors.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "command_pcb_export_base.h"
#include <cli/exit_codes.h>
#include <kiface_base.h>
#include <bitset>
#include <layer_ids.h>
#include <string_utils.h>

#include <macros.h>
#include <wx/tokenzr.h>
#include <wx/crt.h>

CLI::PCB_EXPORT_BASE_COMMAND::PCB_EXPORT_BASE_COMMAND( const std::string& aName,
                                                       bool               aInputIsDir,
                                                       bool               aOutputIsDir ) :
        COMMAND( aName )
{
    m_selectedLayersSet = false;
    m_requireLayers = false;
    m_hasLayerArg = false;
    // 调用了 addCommonArgs 函数，该函数添加了一些通用的参数，
    // 参数是根据 aInputIsDir 和 aOutputIsDir 的值而确定的。
    addCommonArgs( true, true, aInputIsDir, aOutputIsDir );

    // Build list of layer names and their layer mask:
    for( int layer = 0; layer < PCB_LAYER_ID_COUNT; ++layer )
    {
        // 获取图层名称的未翻译版本
        std::string untranslated = TO_UTF8( wxString( LSET::Name( PCB_LAYER_ID( layer ) ) ) );

        //m_layerIndices[untranslated] = PCB_LAYER_ID( layer );

        // Add layer name used in pcb files
        m_layerMasks[untranslated] = LSET( PCB_LAYER_ID( layer ) );
        // Add layer name using GUI canonical layer name
        // 使用 GUI 规范的图层名称，将图层名称和对应的图层掩码添加到 m_layerGuiMasks 中
        m_layerGuiMasks[ TO_UTF8(LayerName( layer ) ) ] = LSET( PCB_LAYER_ID( layer ) );
    }

    // Add list of grouped layer names used in pcb files
    m_layerMasks["*"] = LSET::AllLayersMask();
    m_layerMasks["*.Cu"] = LSET::AllCuMask();
    m_layerMasks["*In.Cu"] = LSET::InternalCuMask();
    m_layerMasks["F&B.Cu"] = LSET( 2, F_Cu, B_Cu );
    m_layerMasks["*.Adhes"] = LSET( 2, B_Adhes, F_Adhes );
    m_layerMasks["*.Paste"] = LSET( 2, B_Paste, F_Paste );
    m_layerMasks["*.Mask"] = LSET( 2, B_Mask, F_Mask );
    m_layerMasks["*.SilkS"] = LSET( 2, B_SilkS, F_SilkS );
    m_layerMasks["*.Fab"] = LSET( 2, B_Fab, F_Fab );
    m_layerMasks["*.CrtYd"] = LSET( 2, B_CrtYd, F_CrtYd );

    // Add list of grouped layer names using GUI canonical layer names
    m_layerGuiMasks["*.Adhesive"] = LSET( 2, B_Adhes, F_Adhes );
    m_layerGuiMasks["*.Silkscreen"] = LSET( 2, B_SilkS, F_SilkS );
    m_layerGuiMasks["*.Courtyard"] = LSET( 2, B_CrtYd, F_CrtYd );
}


LSEQ CLI::PCB_EXPORT_BASE_COMMAND::convertLayerStringList( wxString& aLayerString,
                                                           bool& aLayerArgSet ) const
{
    // 创建了一个空的 LSEQ 类型变量 layerMask，用于存储图层掩码序列。
    LSEQ layerMask;

    // 使用 wxStringTokenizer 对图层名称字符串进行分词，分隔符为逗号 ,。
    if( !aLayerString.IsEmpty() )
    {
        // 将 wxString 转换为 std::string 类型的 token。
        // 首先检查 token 是否在 m_layerMasks 中，即检查是否是 PCB 文件中使用的规范图层名称。
        wxStringTokenizer layerTokens( aLayerString, "," );

        while( layerTokens.HasMoreTokens() )
        {
            std::string token = TO_UTF8( layerTokens.GetNextToken() );

            // Search for a layer name in canonical layer name used in .kicad_pcb files:
            if( m_layerMasks.count( token ) )
            {
                for( PCB_LAYER_ID layer : m_layerMasks.at( token ).Seq() )
                    layerMask.push_back( layer );

                aLayerArgSet = true;
            }
            // Search for a layer name in canonical layer name used in GUI (not translated):
            // 同样将对应的图层掩码序列添加到 layerMask 中，并将 aLayerArgSet 设置为 true。
            else if( m_layerGuiMasks.count( token ) )
            {
                for( PCB_LAYER_ID layer : m_layerGuiMasks.at( token ).Seq() )
                    layerMask.push_back( layer );

                aLayerArgSet = true;
            }
            else
            {
                wxFprintf( stderr, _( "Invalid layer name \"%s\"\n" ), token );
            }
        }
    }

    return layerMask;
}

// 用于向命令行参数解析器中添加图层参数。
void CLI::PCB_EXPORT_BASE_COMMAND::addLayerArg( bool aRequire )
{
    m_argParser.add_argument( "-l", ARG_LAYERS )
            .default_value( std::string() )
            .help( UTF8STDSTR(
                    _( "Comma separated list of untranslated layer names to include such as "
                       "F.Cu,B.Cu" ) ) )
            .metavar( "LAYER_LIST" );
    // 将 m_hasLayerArg 设置为 true，表示存在图层参数。
    m_hasLayerArg = true;
    // 传入的参数 aRequire，用于指示是否需要图层参数。
    m_requireLayers = aRequire;
}

// 函数执行实际的 PCB 导出操作。
int CLI::PCB_EXPORT_BASE_COMMAND::doPerform( KIWAY& aKiway )
{
    // 检查是否存在图层参数 m_hasLayerArg。
    if( m_hasLayerArg )
    {
        wxString layers = From_UTF8( m_argParser.get<std::string>( ARG_LAYERS ).c_str() );
        // 获取命令行中指定的图层列表，并调用 convertLayerStringList 函数将其转换为图层掩码序列。
        LSEQ layerMask = convertLayerStringList( layers, m_selectedLayersSet );

        if( m_requireLayers && layerMask.size() < 1 )
        {
            wxFprintf( stderr, _( "At least one layer must be specified\n" ) );
            return EXIT_CODES::ERR_ARGS;
        }

        m_selectedLayers = layerMask;
    }

    return EXIT_CODES::OK;
}
