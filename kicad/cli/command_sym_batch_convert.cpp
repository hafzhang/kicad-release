/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2022 Mark Roszko <mark.roszko@gmail.com>
 * Copyright (C) 1992-2023 KiCad Developers, see AUTHORS.txt for contributors.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "command_sym_batch_convert.h"
#include <cli/exit_codes.h>
#include "jobs/job_sym_batch_convert.h"
#include <kiface_base.h>
#include <layer_ids.h>
#include <wx/crt.h>
#include <wx/dir.h>

#include <macros.h>


CLI::SYM_BATCH_CONVERT_COMMAND::SYM_BATCH_CONVERT_COMMAND() : COMMAND( "batchconvert" )
{
    addCommonArgs( true, true, false, false );

    m_argParser.add_description( UTF8STDSTR( _( "Convert other symbol library to the current "
                                                "kicad version" ) ) );

}


int CLI::SYM_BATCH_CONVERT_COMMAND::doPerform( KIWAY& aKiway )
{
    std::unique_ptr<JOB_SYM_BATCH_CONVERT> symsJob = std::make_unique<JOB_SYM_BATCH_CONVERT>( true );

    symsJob->m_libraryDirectory = m_argInput;
    symsJob->m_outputDirectory = m_argOutput;

    if( !wxDirExists( symsJob->m_libraryDirectory ) )
    {
        wxFprintf( stderr, _( "Symbol library directory does not exist or is not accessible\n" ) );
        return EXIT_CODES::ERR_INVALID_INPUT_FILE;
    }

    int exitCode = aKiway.ProcessJob( KIWAY::FACE_SCH, symsJob.get() );

    return exitCode;
}
