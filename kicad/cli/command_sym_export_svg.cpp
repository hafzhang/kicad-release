/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2022 Mark Roszko <mark.roszko@gmail.com>
 * Copyright (C) 1992-2023 KiCad Developers, see AUTHORS.txt for contributors.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "command_sym_export_svg.h"
#include <cli/exit_codes.h>
#include "jobs/job_sym_export_svg.h"
#include <kiface_base.h>
#include <layer_ids.h>
#include <string_utils.h>
#include <wx/crt.h>

#include <macros.h>
#include <wx/tokenzr.h>


#define ARG_NO_BACKGROUND_COLOR "--no-background-color"
#define ARG_SYMBOL "--symbol"
#define ARG_INC_HIDDEN_PINS "--include-hidden-pins"
#define ARG_INC_HIDDEN_FIELDS "--include-hidden-fields"


CLI::SYM_EXPORT_SVG_COMMAND::SYM_EXPORT_SVG_COMMAND() : COMMAND( "svg" )
{
//     addCommonArgs 方法用于添加命令的常见参数。
    addCommonArgs( true, true, false, true );

    m_argParser.add_description( UTF8STDSTR( _( "Exports the symbol or entire symbol library "
                                                "to SVG" ) ) );
        
        // add_argument 方法用于向命令行解析器添加特定的参数选项。
    m_argParser.add_argument( "-t", ARG_THEME )
            .default_value( std::string() )
            .help( UTF8STDSTR( _( "Color theme to use (will default to symbol editor "
                                  "settings)" ) ) )
            .metavar( "THEME_NAME" );

    m_argParser.add_argument( "-s", ARG_SYMBOL )
            .default_value( std::string() )
            .help( UTF8STDSTR( _( "Specific symbol to export within the library" ) ) )
            .metavar( "SYMBOL" );
        // "ARG_BLACKANDWHITE" 参数用于指定导出的SVG是否为黑白模式，
    m_argParser.add_argument( ARG_BLACKANDWHITE )
            .help( UTF8STDSTR( _( ARG_BLACKANDWHITE_DESC ) ) )
            .flag();
        // "ARG_INC_HIDDEN_PINS" 参数用于指定是否包括隐藏的引脚
    m_argParser.add_argument( ARG_INC_HIDDEN_PINS )
            .help( UTF8STDSTR( _( "Include hidden pins" ) ) )
            .flag();
        // "ARG_INC_HIDDEN_FIELDS" 参数用于指定是否包括隐藏的字段
    m_argParser.add_argument( ARG_INC_HIDDEN_FIELDS )
            .help( UTF8STDSTR( _( "Include hidden fields" ) ) )
            .flag();
}


int CLI::SYM_EXPORT_SVG_COMMAND::doPerform( KIWAY& aKiway )
{
        // std::make_unique 函数用于创建一个新的 JOB_SYM_EXPORT_SVG 对象，并将其指针分配给 svgJob。
        // true 参数传递给 JOB_SYM_EXPORT_SVG 的构造函数，可能用于指示该作业是否是异步执行的。
    std::unique_ptr<JOB_SYM_EXPORT_SVG> svgJob = std::make_unique<JOB_SYM_EXPORT_SVG>( true );

        // 分别将命令行参数中的输入路径、输出目录、黑白模式、指定的符号名称、
        // 是否包含隐藏字段、是否包含隐藏引脚等属性设置给 svgJob 对象。
    svgJob->m_libraryPath = m_argInput;
    svgJob->m_outputDirectory = m_argOutput;
    svgJob->m_blackAndWhite = m_argParser.get<bool>( ARG_BLACKANDWHITE );
    svgJob->m_symbol = From_UTF8( m_argParser.get<std::string>( ARG_SYMBOL ).c_str() );
    svgJob->m_includeHiddenFields = m_argParser.get<bool>( ARG_INC_HIDDEN_FIELDS );
    svgJob->m_includeHiddenPins = m_argParser.get<bool>( ARG_INC_HIDDEN_PINS );
// 使用 wxFile::Exists 函数检查指定的符号文件路径是否存在。
    if( !wxFile::Exists( svgJob->m_libraryPath ) )
    {
        wxFprintf( stderr, _( "Symbol file does not exist or is not accessible\n" ) );
        return EXIT_CODES::ERR_INVALID_INPUT_FILE;
    }

    svgJob->m_colorTheme = From_UTF8( m_argParser.get<std::string>( ARG_THEME ).c_str() );
        // 使用 aKiway.ProcessJob 方法执行作业。该方法接受两个参数，第一个参数是指定作业的面（FACE），
        // 这里是 KIWAY::FACE_SCH ，表示作业将在符号编辑器的上下文中执行；第二个参数是要执行的作业，
        // 即 svgJob 智能指针所管理的 JOB_SYM_EXPORT_SVG 对象。
    int exitCode = aKiway.ProcessJob( KIWAY::FACE_SCH, svgJob.get() );

    return exitCode;
}
