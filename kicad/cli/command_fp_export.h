/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2022 Mark Roszko <mark.roszko@gmail.com>
 * Copyright (C) 1992-2022 KiCad Developers, see AUTHORS.txt for contributors.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMAND_FP_EXPORT_H
#define COMMAND_FP_EXPORT_H

#include "command.h"

// 使用命名空间可以避免名称冲突，并使代码更模块化。
namespace CLI
{
struct FP_EXPORT_COMMAND : public COMMAND
{
    // 构造函数中的代码用于初始化命令的名称和参数解析器等信息。
    FP_EXPORT_COMMAND() : COMMAND( "export" )
    {
        m_argParser.add_description( UTF8STDSTR( _( "Export utilities (svg)" ) ) );
    }
};
}

#endif