/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2022 Mark Roszko <mark.roszko@gmail.com>
 * Copyright (C) 1992-2023 KiCad Developers, see AUTHORS.txt for contributors.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "command.h"
#include <cli/exit_codes.h>
#include <wx/crt.h>
#include <macros.h>
#include <string_utils.h>

#include <sstream>

// 接受一个字符串参数 aName，表示命令的名称。
CLI::COMMAND::COMMAND( const std::string& aName ) :
        // 初始化了成员变量
        m_name( aName ),
        // 初始化了成员变量 m_argParser，它是一个参数解析器对象。
        m_argParser( aName, "", argparse::default_arguments::none ),
        // 命令是否具有输入参数、输出参数、绘图工作表参数、定义参数以及输出参数是否需要目录。
        m_hasInputArg( false ),
        m_hasOutputArg( false ),
        m_hasDrawingSheetArg( false ),
        m_hasDefineArg( false ),
        m_outputArgExpectsDir( false )

{
    // 用于表示帮助信息。参数的标识符是 ARG_HELP_SHORT 和 ARG_HELP
    m_argParser.add_argument( ARG_HELP_SHORT, ARG_HELP )
                .default_value( false )
                .help( UTF8STDSTR( ARG_HELP_DESC ) )
                .implicit_value( true )
                .nargs( 0 );
}

// 用于打印命令的帮助信息，
void CLI::COMMAND::PrintHelp()
{
    // ss 用于存储参数解析器的信息。
    std::stringstream ss;
    ss << m_argParser;
    wxPrintf( From_UTF8( ss.str().c_str() ) );
}

// Perform() 方法用于执行命令的实际操作，并处理命令中的参数。
int CLI::COMMAND::Perform( KIWAY& aKiway )
{
    
    if( m_argParser[ ARG_HELP ] == true )
    {
        PrintHelp();

        return 0;
    }
    // 将输入参数的值存储在 m_argInput 变量中。
    if ( m_hasInputArg )
    {
        m_argInput = From_UTF8( m_argParser.get<std::string>( ARG_INPUT ).c_str() );
    }
    // 将输出参数的值存储在 m_argOutput 变量中。
    if( m_hasOutputArg )
    {
        m_argOutput = From_UTF8( m_argParser.get<std::string>( ARG_OUTPUT ).c_str() );
    }
    // 将其值存储在 m_argDrawingSheet 变量中。
    if( m_hasDrawingSheetArg )
    {
        m_argDrawingSheet = From_UTF8( m_argParser.get<std::string>( ARG_DRAWING_SHEET ).c_str() );
    }


    if( m_hasDefineArg )
    {
        auto defines = m_argParser.get<std::vector<std::string>>( ARG_DEFINE_VAR_LONG );

        for( const std::string& def : defines )
        {
            wxString      str = From_UTF8( def.c_str() );
            wxArrayString bits;
            wxStringSplit( str, bits, wxS( '=' ) );

            if( bits.Count() == 2 )
            {
                m_argDefineVars[bits[0]] = bits[1];
            }
            else
            {
                return EXIT_CODES::ERR_ARGS;
            }
        }
    }
    // 调用 doPerform() 方法执行实际的命令操作，并返回执行结果的错误码。
    return doPerform( aKiway );
}


int CLI::COMMAND::doPerform( KIWAY& aKiway )
{
    // default case if we aren't overloaded, just print the help
    PrintHelp();

    return EXIT_CODES::OK;
}

// 它接受四个布尔参数，分别表示是否具有输入参数、输出参数，以及输入参数和输出参数是否期望是目录。
void CLI::COMMAND::addCommonArgs( bool aInput, bool aOutput, bool aInputIsDir, bool aOutputIsDir )
{
    m_hasInputArg = aInput;
    m_hasOutputArg = aOutput;
    m_outputArgExpectsDir = aOutputIsDir;
    // 用于根据参数设置是否具有输入参数以及输入参数是否为目录，向命令添加相应的参数描述。
    if( aInput )
    {
        if( aInputIsDir )
        {
            m_argParser.add_argument( ARG_INPUT )
                    .help( UTF8STDSTR( _( "Input directory" ) ) )
                    .metavar( "INPUT_DIR" );
        }
        else
        {
            m_argParser.add_argument( ARG_INPUT )
                        .help( UTF8STDSTR( _( "Input file" ) ) )
                        .metavar( "INPUT_FILE" );
        }
    }

    if( aOutput )
    {
        if( aOutputIsDir )
        {
            m_argParser.add_argument( "-o", ARG_OUTPUT )
                    .default_value( std::string() )
                    .help( UTF8STDSTR( _( "Output directory" ) ) )
                    .metavar( "OUTPUT_DIR" );
        }
        else
        {
            m_argParser.add_argument( "-o", ARG_OUTPUT )
                    .default_value( std::string() )
                    .help( UTF8STDSTR( _( "Output file" ) ) )
                    .metavar( "OUTPUT_FILE" );
        }
    }
}

// 用于向命令添加绘图工作表参数的描述。
void CLI::COMMAND::addDrawingSheetArg()
{
    m_hasDrawingSheetArg = true;

    m_argParser.add_argument( ARG_DRAWING_SHEET )
            .default_value( std::string() )
            .help( UTF8STDSTR( _( "Path to drawing sheet, this overrides any existing project "
                                  "defined sheet when used" ) ) )
            .metavar( "SHEET_PATH" );
}

// 用于向命令添加定义参数的描述。
void CLI::COMMAND::addDefineArg()
{
    m_hasDefineArg = true;
    // 使用 m_argParser.add_argument() 方法向参数解析器添加了一个参数描述，这个参数用于表示定义变量
    m_argParser.add_argument( ARG_DEFINE_VAR_LONG, ARG_DEFINE_VAR_SHORT )
            .default_value( std::vector<std::string>() )
            .append()
            .help( UTF8STDSTR(
                    _( "Overrides or adds project variables, can be used multiple times to "
                       "declare multiple variables."
                       "\nUse in the format of '--define-var key=value' or '-D key=value'" ) ) )
            .metavar( "KEY=VALUE" );
}
