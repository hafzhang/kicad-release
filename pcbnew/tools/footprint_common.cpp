#include <wx/dir.h>
#include <wx/filename.h>
#include "footprint_common.h"

wxArrayString GetAllDirectoriesInDir(const wxString& strDir) {
    wxArrayString dirs;
    wxDir dir(strDir);

    if (dir.IsOpened()) {
        wxString dirName;
        if (dir.GetFirst(&dirName, wxEmptyString, wxDIR_DIRS)) {
            do {
                if (dirName != "." && dirName != "..") {
                    dirs.Add(dirName); // 添加子目录名
                }
            } while (dir.GetNext(&dirName)); // 正确使用 GetNext
        }
    }
    for (const wxString& dir : dirs) {
        wxPrintf(wxT("%s\n"), dir.c_str());
    }
    return dirs;
}

