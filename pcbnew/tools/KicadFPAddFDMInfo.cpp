#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "KicadFPAddFDMInfo.h"
#include "footprint.h"
#include "pad.h"
// #include "plugins/kicad/pcb_plugin.h"
// #include "../plugins/kicad/pcb_parser.h"
#include <pcb_io/kicad_sexpr/pcb_io_kicad_sexpr.h>
#include <pcb_io/kicad_sexpr/pcb_io_kicad_sexpr_parser.h>
#include "pcb_shape.h"
#include "zone.h"
#include "footprint_edit_frame.h"
// #include "pcb_tool_base.h"
#include <tools/pcb_tool_base.h>
#include <board_commit.h>
#include <../../../include/tool/tool_manager.h>

#include "pad_tool.h" 

#include <../../../thirdparty/nlohmann_json/nlohmann/json.hpp>
#include "board_design_settings.h"
#include <iomanip>
#include "footprint_common.h"
#include <tools/pcb_tool_base.h>
#include "LCFPChangeToKicadFP.h"

#include <tools/pcb_point_editor.h>
#include <board.h>



using namespace std;
using nlohmann::json;

//立创专业版EDA封装文件数据扩大倍数
#define LCFPExpendNum 1000000 * 0.0254

#define DFMExpendNum 1000000

//Kicad precision
#define LCFPExpend 1000000

KicadFPAddFDMInfo::KicadFPAddFDMInfo( BOARD* board )
{
    m_LCFPSingleFullPath = "";
    m_board = board;
    m_KicadFPObj = nullptr;
    m_FPCollection = new LCFPInforCollection();
    m_DeviceInfor = new LCDeviceInfor();

}

KicadFPAddFDMInfo::~KicadFPAddFDMInfo()
{
    if( nullptr != m_FPCollection )
    {
        delete m_FPCollection;
    }

     if( nullptr != m_DeviceInfor )
    {
        delete m_DeviceInfor;
    }
}

//解析立创EDA专业版封装文件，并存储数据至Kicad数据结构
int KicadFPAddFDMInfo::importLCFP( string strInFileFullPath, int fileType )
{
    int resultID = 0;
    if( 1 > strInFileFullPath.size() )
    {
        return resultID;
    }

    m_KicadFPObj = new FOOTPRINT( m_board );
    std::vector<std::string> vecStrTemp = Split( strInFileFullPath, "\\" );
    std::string              strFPName = vecStrTemp.back();
    strFPName = strFPName.substr( 0, strFPName.size() - 5 );
    footprintName = strFPName;

    //设置Kicad封装所在文件夹名称和封装名称
    m_KicadFPObj->SetFPID( LIB_ID( "LCPROFP", strFPName ) );

    vector<std::string> vecLCFPLine;

    resultID = readLCFPFileByLine( strInFileFullPath, vecLCFPLine );

    resultID = parseLines( vecLCFPLine );


    //获取解析数据
    m_FPCollection->m_kicadFP = m_KicadFPObj;
    GetCollectInfor();
    praseDFMJsonData();
    setReferencePosit();
    if( pinID != "" ){
        if( polarity =="" ){
            drawingFsilkPin1( pinID );
        }else{
            drawingFsilkPolarity( pinID );
        }
    }


    return resultID;
}

//导出Kicad封装文件
int KicadFPAddFDMInfo::exportKicadFP( string strOutFileFullPath )
{
    return 1;
}

//按行读取立创封装文件
int KicadFPAddFDMInfo::readLCFPFileByLine( string               strInFileFullPath,
                                             vector<std::string>& vecLCFPLines )
{
    int      resultID = 1;
    ifstream readFile;
    readFile.open( strInFileFullPath, ios::in );

    if( readFile.is_open() )
    {
        string str;
        while( getline( readFile, str ) )
        {
            vecLCFPLines.push_back( str );
        }
    }
    else
    {
        resultID = 2;
    }

    readFile.close();

    return resultID;
}

//解析行数据集合
int KicadFPAddFDMInfo::parseLines( std::vector<std::string> vecLCFPLines )
{
    int resultID = 0;
    size_t count = vecLCFPLines.size();
    for( size_t i = 0; i < vecLCFPLines.size(); i++ )
    {
        string strLine = vecLCFPLines[i];

        //去除首尾的[]
        strLine = spp( strLine, "[", "" );
        strLine = spp( strLine, "]", "" );
        strLine = spp( strLine, "\"", "" );

        vector<string> vecSplit = Split( strLine, "," );

        ////去除双引号
        //vecStrAnalyse( vecSplit );

        //筛除无效数据
        if( 1 > vecSplit.size() )
        {
            continue;
        }
        else
        {
            if( strLine.size() <= vecSplit[1].find( "e" ) )
            {
                continue;
            }
        }

        //解析数据
        if( strLine.size() > vecSplit[0].find( "PAD" ) )
        {
            //解析PAD
            resultID = parsePad( vecSplit );
        }
        else if( strLine.size() > vecSplit[0].find( "POLY" ) )
        {
            //解析POLY
            resultID = parsePOLY( vecSplit );
        }
        else if( strLine.size() > vecSplit[0].find( "FILL" ) )
        {
            //解析FILL
            // resultID = parseFILL( vecSplit );
        }
        else if( strLine.size() > vecSplit[0].find( "STRING" ) )
        {
            //解析STRING(string)
            // resultID = parseSTRING( vecSplit );
        }
        else if( strLine.size() > vecSplit[0].find( "REGION" ) )
        {
            //解析REGION
            // resultID = parseREGION( vecSplit );
        }
        else if( strLine.size() > vecSplit[0].find( "ATTR" ) )
        {
            //解析REGION
            resultID = parseATTR( vecSplit );
        }

        // if (i == count - 2)
        // {
            
            
        //     // praseSilkOutline( m_shapesInfo, m_pointsInfo );
        // }
    }

    return resultID;
}

//wx Get All File in Dir( -sxl)
wxArrayString KicadFPAddFDMInfo::GetAllFilesInDir1(const wxString& strDir)
{
    wxDir dir;
    wxArrayString fileLists;
    wxString      fileSpec = wxT( "*.efoo" );
    wxString      fileSpecJson = wxT( "*.json" );
    int           numFilesFound;
    if( dir.Open(strDir))
    {
        numFilesFound = dir.GetAllFiles( strDir, &fileLists, fileSpec );
        numFilesFound = dir.GetAllFiles( strDir, &fileLists, fileSpecJson );
    }

    return fileLists;
}

int KicadFPAddFDMInfo::praseDFMJsonData(){
    wxString fdmJsonData = "E://dfmoutput";
    wxString fullPath = fdmJsonData + "//" + footprintName.c_str();
    wxArrayString files = GetAllFilesInDir1( fullPath );
    for (const wxString& file : files) {
        wxFileName fn(file);
        wxString fnName = fn.GetFullName();
        if( fnName.ToStdString() == "MGL.json" ){
            wxString filefullPath = fn.GetFullPath();
            wxString filePath = filefullPath;

            std::ifstream file(filePath.ToStdString());
            if (!file.is_open()) {
                wxLogError("Failed to open the file: %s", filePath);
                return 0;
            }

            // 读取文件内容
            std::string jsonData((std::istreambuf_iterator<char>(file)),
                                std::istreambuf_iterator<char>());

            // 解析 JSON 数据
            try {
                json j = json::parse(jsonData);

                // 处理 JSON 数据
                json  body = j["body"];
                std::vector<std::string> body_string;
                for (const auto& str : body) {
                    body_string.push_back(str);
                }

                std::vector<std::string> values;
                for (const auto& item : body_string) {
                    std::istringstream iss(item);
                    std::string token;
                    while (std::getline(iss, token, ';')) {
                        std::istringstream pointStream(token);
                        std::string coord;

                        // 再按逗号分割，获取每个坐标值
                        while (std::getline(pointStream, coord, ',')) {
                            values.push_back(coord); // 将字符串转换为 double 并添加到 values 中
                        }
                    }                    
                }
                if(body_string.size() == 1){
                    drawingFFabOutline(values);
                }else{
                    int iin = body_string.size();
                }

                std::cout << "Body: " << body.dump(4) << std::endl;

                // 获取 "pin" 对象
                json pin = j["pin"];
                if (pin.is_null()){
                    continue;
                }
                else{
                    pinID = pin["pinID"];
                    polarity = pin["polarity"];

                    // if( polarity =="" ){
                    //     drawingFsilkPin1( pinID );
                    // }else{
                    //     drawingFsilkPolarity( pinID );
                    // }
                }

            } catch (json::parse_error& e) {
                wxLogError("JSON parse error: %s", e.what());
            }

        }
    }


return 0;
}


// 解析外框大小
int KicadFPAddFDMInfo::drawingFFabOutline( std::vector<std::string>& pointsInfo )
{
    PCB_SHAPE* shape = new PCB_SHAPE( m_KicadFPObj );
    std::string F_silk = "3";
    shape->SetLayer( F_Fab );


    // SHAPE_LINE_CHAIN lineChain;


    if( pointsInfo[0] == "1"){
        shape->SetShape( SHAPE_T::POLY );
        std::vector<VECTOR2I> pts;
        for (size_t i = 0; i < pointsInfo.size(); i += 3) {
        // 检查是否有足够的元素来形成一个点
            VECTOR2I   point;
            point.x = DFMRetainTwoDegree( stof(pointsInfo[i + 1]) );
            point.y = DFMRetainTwoDegree( stof(pointsInfo[i + 2]) );
            pts.push_back(point);
            // lineChain.Append(point);
        }
        shape->SetFilled( false );
        shape->SetPolyPoints( pts );
        shape->SetWidth( RetainTwoDegree( stof( "4.724" ) ) );
        m_KicadFPObj->Add( shape, ADD_MODE::APPEND, true );
        SHAPE_POLY_SET outline = shape->GetPolyShape();

        drawingFsilkOutline( outline );

    }else{
        string graph = pointsInfo[0];
    }
    return 0;

}

bool NotEqual( double v1, double v2 )
{
    double  m_Epsilon = 1E-2;
    double dDiff = fabs( v1 - v2 );
    return dDiff > m_Epsilon;
}

bool IsEqual( double v1, double v2 )
{
    double  m_Epsilon = 1E-2;
    return fabs( v1 - v2 ) < m_Epsilon ? TRUE : FALSE;
}


bool Segment2SegmentIntersect( double x1, double y1, double x2, double y2, double x3,
                                              double y3, double x4, double y4, double& ix,
                                              double& iy )
{
    double ax = x2 - x1;

    double lowerx = ( ax < 0.0 ) ? x2 : x1;
    double upperx = ( ax < 0.0 ) ? x1 : x2;
    double bx = x3 - x4;
    if( bx > 0.0 )
    {
        if( ( upperx < x4 ) || ( x3 < lowerx ) )
        {
            return FALSE;
        }
    }
    else if( ( upperx < x3 ) || ( x4 < lowerx ) )
    {
        return FALSE;
    }

    double ay = y2 - y1;
    double uppery = ( ay < 0.0 ) ? y1 : y2;
    double lowery = ( ay < 0.0 ) ? y2 : y1;

    double by = y3 - y4;
    if( by > 0.0 )
    {
        if( ( uppery < y4 ) || ( y3 < lowery ) )
        {
            return FALSE;
        }
    }
    else if( ( uppery < y3 ) || ( y4 < lowery ) )
    {
        return FALSE;
    }

    double cx = x1 - x3;
    double cy = y1 - y3;
    double d = ( by * cx ) - ( bx * cy );
    double f = ( ay * bx ) - ( ax * by );

    if( f > 0.0 )
    {
        if( ( d < 0.0 ) || ( d > f ) )
        {
            return FALSE;
        }
    }
    else if( ( d > 0.0 ) || ( d < f ) )
    {
        return FALSE;
    }

    double e = ( ax * cy ) - ( ay * cx );
    if( f > 0.0 )
    {
        if( ( e < 0.0 ) || ( e > f ) )
        {
            return FALSE;
        }
    }
    else if( ( e > 0.0 ) || ( e < f ) )
    {
        return FALSE;
    }

    double ratio = ( ax * -by ) - ( ay * -bx );
    if( NotEqual( ratio, 0.0 ) )
    {
        ratio = ( ( cy * -bx ) - ( cx * -by ) ) / ratio;
        ix = x1 + ( ratio * ax );
        iy = y1 + ( ratio * ay );
    }
    else
    {
        if( IsEqual( ( ax * -cy ), ( -cx * ay ) ) )
        {
            ix = x3;
            iy = y3;
        }
        else
        {
            ix = x4;
            iy = y4;
        }
    }
    return TRUE;
}



// 函数：检查两个线段是否相交，并返回相交点
bool getLineIntersection(const VECTOR2I& p1, const VECTOR2I& p2, const VECTOR2I& p3, const VECTOR2I& p4, VECTOR2I& intersection) {
 
    double ix, iy;
    bool intersects = Segment2SegmentIntersect(
                 static_cast<double>(p1.x), static_cast<double>(p1.y),
                static_cast<double>(p2.x), static_cast<double>(p2.y),
                static_cast<double>(p3.x), static_cast<double>(p3.y),
                static_cast<double>(p4.x), static_cast<double>(p4.y),
                ix, iy );
    if (intersects) {
        intersection.x = static_cast<int>(ix);
        intersection.y = static_cast<int>(iy);
    }
    return intersects;
}


// 函数：检查点是否在方框内
bool isPointInBox(const VECTOR2I& point, const BOX2I& box) {
    //注意坐标系
    int leftX = box.GetLeft();
    int rightX = box.GetRight();
    int bottomY = box.GetBottom();
    int topY = box.GetTop();

    bool result = point.x >= leftX && point.x <= rightX &&
                  point.y <= bottomY && point.y >= topY;    

    return result;    
}

bool comparePoints(const VECTOR2I& a, const VECTOR2I& b) {
    if (a.x != b.x) return a.x < b.x;
    return a.y < b.y;
}


struct Direction {
    double x, y;
};
// 线段的终点，缩短制定的距离
VECTOR2I shortenLineEnd(const VECTOR2I& pStart, const VECTOR2I& pEnd ) {
    double shortenBy = RetainTwoDegree( stof( "4.724" ) );
    Direction direction =  {static_cast<double>(pEnd.x - pStart.x), static_cast<double>(pEnd.y - pStart.y)};
    double lineLength = std::sqrt( direction.x * direction.x + direction.y * direction.y );

    if (lineLength == 0) {
        return pEnd; // Start and end are the same, nothing to shorten
    }

    double shorteningFactor = (lineLength - shortenBy) / lineLength;

    if (shorteningFactor < 0) {
        // If the shortening factor is negative, the shortening length is more than the original length
        // This could be handled as a special case depending on your needs
        return pStart; // or handle this case as needed
    }
    VECTOR2I newEnd = {
        static_cast<int>(pStart.x + direction.x * shorteningFactor),
        static_cast<int>(pStart.y + direction.y * shorteningFactor)
    };
    return newEnd;
}


// 函数：返回线段在方框外面的部分
void getSegmentOutsideRectangle(const SEG& line, const BOX2I& rect, std::vector<SEG>& outsideSegments) {
    VECTOR2I intersection1, intersection2;
    std::vector<VECTOR2I> intersections;

    VECTOR2I topLeft = rect.GetOrigin();
    VECTOR2I bottomRight = rect.GetEnd();
    VECTOR2I rectPoints[4] = {
        topLeft,
        { bottomRight.x, topLeft.y },
        bottomRight,
        { topLeft.x, bottomRight.y }
    };

    bool startInside = isPointInBox(line.A, rect);
    bool endInside = isPointInBox(line.B, rect);
    // 如果起点和终点都在方框内部，那么没有部分在外部
    if (startInside && endInside) {
        return;
    }
    // 情况2: 一个点在方框内部，一个点在外部
    else if( startInside || endInside){
        for (int i = 0; i < 4; ++i) {
            VECTOR2I next = rectPoints[(i + 1) % 4];
            if (getLineIntersection(line.A, line.B, rectPoints[i], next, intersection1)) {
                if (!isPointInBox(line.A, rect)) {
                    outsideSegments.push_back({ line.A, shortenLineEnd(line.A, intersection1) });
                }
                if (!isPointInBox(line.B, rect)) {
                    outsideSegments.push_back({  shortenLineEnd(line.B, intersection1) , line.B });
                }
            }
        }
    }
    else{
        bool intersects = false;
        // 情况3: 两个端点都在外部

        for (int i = 0; i < 4; ++i) {
            VECTOR2I next = rectPoints[(i + 1) % 4];

            if (getLineIntersection(line.A, line.B, rectPoints[i], next, intersection1)) {
                intersects = true;
                intersections.push_back(intersection1);
            }
        }
        // 情况3: 两个端点都在外部，但是与方框有两个交点
        if (intersects && intersections.size() == 2) {
            // 排序交点
            std::sort(intersections.begin(), intersections.end(), comparePoints);
            // 根据交点的顺序添加线段
            if (line.A.x < line.B.x || (line.A.x == line.B.x && line.A.y < line.B.y)) {
                outsideSegments.push_back({ line.A, shortenLineEnd(line.A, intersections[0])  });
                outsideSegments.push_back({ shortenLineEnd(line.B, intersections[1]) , line.B });
            } else {
                outsideSegments.push_back({ line.A, shortenLineEnd(line.A, intersections[1]) });
                outsideSegments.push_back({ shortenLineEnd(line.B, intersections[0]), line.B });
            }
        }
        //情况4: 与方框没有相交
        if (!intersects && !isPointInBox(line.A, rect) && !isPointInBox(line.B, rect)) {
            outsideSegments.push_back(line);
        }
    }
    return;
}


// 添加丝印层外框
int KicadFPAddFDMInfo::drawingFsilkOutline( SHAPE_POLY_SET outline )
{
    std::string F_silk = "3";
    int intervalWidth = RetainTwoDegree( stof( "8.661" ) );
    int aMaxError = 1;
    outline.Inflate( intervalWidth, CORNER_STRATEGY::ALLOW_ACUTE_CORNERS, aMaxError );
    outline.Fracture( SHAPE_POLY_SET::PM_FAST );

    BOX2I PadsBbox = m_KicadFPObj->GetFpPadsLocalBbox();
    PadsBbox.Inflate(RetainTwoDegree( stof( "3.149" ) ), RetainTwoDegree( stof( "3.149" ) ) );

    vector<SEG> allPoly;  
    for( int poly_index = 0; poly_index < outline.OutlineCount(); ++poly_index )
    {
        const SHAPE_LINE_CHAIN& polygon = outline.Outline(poly_index);
        //  遍历多边形中的所有边
        for( int i = 0; i < polygon.SegmentCount(); ++i )
        {
            const SEG& poly = polygon.Segment(i);
            allPoly.push_back(poly); 
        }
    }

    std::vector<SEG> outside;
    for( int poly_index = 0; poly_index < allPoly.size(); ++poly_index ){

        std::vector<SEG> outsideSegments;
        getSegmentOutsideRectangle(allPoly[poly_index], PadsBbox, outsideSegments ) ;
        if( !outsideSegments.empty() ){
            for( size_t i = 0;i< outsideSegments.size(); i++ ){
                outside.push_back(outsideSegments[i]);
            }
        }

    }

    for( int poly_index = 0; poly_index < outside.size(); ++poly_index )
    {
        vector<VECTOR2I> outsidePoint;
        outsidePoint.push_back( outside[poly_index].A );
        outsidePoint.push_back( outside[poly_index].B );

        PCB_SHAPE* outshape = new PCB_SHAPE( m_KicadFPObj );
        outshape->SetLayer( (PCB_LAYER_ID) LCLayerIDToKicadLayerID( F_silk ) );
        outshape->SetShape( SHAPE_T::SEGMENT );
        // outshape->SetFilled( false );
        outshape->SetStart( outside[poly_index].A );
        outshape->SetEnd( outside[poly_index].B );
        // outshape->SetPolyPoints( outsidePoint );
        outshape->SetWidth( RetainTwoDegree( stof( "4.724" ) ) );
        m_KicadFPObj->Add( outshape, ADD_MODE::APPEND, true );
    }

    return 0;
}




int KicadFPAddFDMInfo::drawingFsilkPolarity( std::string& pinID ){
    
    VECTOR2I   sp;
    VECTOR2I   ep;
    VECTOR2I   hitp;
    VECTOR2I   midp;

    PAD* hitPad = m_KicadFPObj->FindPadByNumber( pinID.c_str() );

    if (hitPad == nullptr) 
        return 0;
    VECTOR2I hitPadPos = hitPad->GetPosition();
    
    // 直径  59.055
    SHAPE_POLY_SET shapeCompound = m_KicadFPObj->GetBoundingHull();

    int angle = static_cast<int>(round( hitPad->GetOrientationDegrees()) );
    // 左边
    if (angle ==90 || angle == 270 ){
        int padSizeY = (hitPad->GetSizeY())/2; // 直径
        sp.x = hitPadPos.x - padSizeY - RetainTwoDegree( stof( "59.055" ) ) - RetainTwoDegree( stof( "7.874" ) );
        sp.y = hitPadPos.y;
        ep.x = hitPadPos.x - padSizeY - RetainTwoDegree( stof( "7.874" ));
        ep.y = hitPadPos.y;
    }else{
        int padSizeX = (hitPad->GetSizeX())/2; // 直径
        sp.x = hitPadPos.x - padSizeX - RetainTwoDegree( stof( "59.055" ) ) - RetainTwoDegree( stof( "7.874" ) );
        sp.y = hitPadPos.y;
        ep.x = hitPadPos.x - padSizeX - RetainTwoDegree( stof( "7.874" ));
        ep.y = hitPadPos.y;
    }
    // 右边
    if ( shapeCompound.Collide(sp) || shapeCompound.Collide(ep) ){
        if (angle ==90 || angle == 270 ){
            int padSizeY = (hitPad->GetSizeY())/2; // 直径
            sp.x = hitPadPos.x + padSizeY + RetainTwoDegree( stof( "59.055" ) ) + RetainTwoDegree( stof( "7.874" ) );
            sp.y = hitPadPos.y;
            ep.x = hitPadPos.x + padSizeY + RetainTwoDegree( stof( "7.874" ));
            ep.y = hitPadPos.y;

        }else{
            int padSizeX = (hitPad->GetSizeX())/2; // 直径
            sp.x = hitPadPos.x + padSizeX + RetainTwoDegree( stof( "59.055" ) ) + RetainTwoDegree( stof( "7.874" ) );
            sp.y = hitPadPos.y;
            ep.x = hitPadPos.x + padSizeX + RetainTwoDegree( stof( "7.874" ));
            ep.y = hitPadPos.y;
        }
        for( int k = 3; k<20; k++ ){
            // 左边
            if (  m_KicadFPObj->HitTest(sp) || m_KicadFPObj->HitTest(ep) ){
                if (angle ==90 || angle == 270 ){
                    int padSizeY = (hitPad->GetSizeY())/2; // 直径
                    sp.x = hitPadPos.x - padSizeY - RetainTwoDegree( stof( "59.055" ) ) - RetainTwoDegree( stof( "7.874" ) *k );
                    sp.y = hitPadPos.y;
                    ep.x = hitPadPos.x - padSizeY - RetainTwoDegree( stof( "7.874" )*k);
                    ep.y = hitPadPos.y;
                }else{
                    int padSizeX = (hitPad->GetSizeX())/2; // 直径
                    sp.x = hitPadPos.x - padSizeX - RetainTwoDegree( stof( "59.055" ) ) - RetainTwoDegree( stof( "7.874" )*k );
                    sp.y = hitPadPos.y;
                    ep.x = hitPadPos.x - padSizeX - RetainTwoDegree( stof( "7.874" )*k);
                    ep.y = hitPadPos.y;
                }
            }
            // 右边
            if ( m_KicadFPObj->HitTest(sp) || m_KicadFPObj->HitTest(ep) ){
                if (angle ==90 || angle == 270 ){
                    int padSizeY = (hitPad->GetSizeY())/2; // 直径
                    sp.x = hitPadPos.x + padSizeY + RetainTwoDegree( stof( "59.055" ) ) + RetainTwoDegree( stof( "7.874" )*k );
                    sp.y = hitPadPos.y;
                    ep.x = hitPadPos.x + padSizeY + RetainTwoDegree( stof( "7.874" )*k );
                    ep.y = hitPadPos.y;

                }else{
                    int padSizeX = (hitPad->GetSizeX())/2; // 直径
                    sp.x = hitPadPos.x + padSizeX + RetainTwoDegree( stof( "59.055" ) ) + RetainTwoDegree( stof( "7.874" )*k );
                    sp.y = hitPadPos.y;
                    ep.x = hitPadPos.x + padSizeX + RetainTwoDegree( stof( "7.874" )*k );
                    ep.y = hitPadPos.y;
                }
            }
        }

    }

    midp.x = (sp.x + ep.x) / 2.0;
    midp.y = (sp.y + ep.y) / 2.0;

    VECTOR2I vsp,vep;
    vsp.x = midp.x;
    vsp.y = midp.y - RetainTwoDegree( stof( "29.527" ) );
    vep.x = midp.x;
    vep.y = midp.y + RetainTwoDegree( stof( "29.528" ) );

    drawingSegment(sp,ep);
    drawingSegment(vsp,vep);
    return 0;
}

int KicadFPAddFDMInfo::drawingSegment(const VECTOR2I& sp, const VECTOR2I& ep){
    PCB_SHAPE* shape = new PCB_SHAPE( m_KicadFPObj );  
    std::string F_silk = "3";
    shape->SetLayer( (PCB_LAYER_ID) LCLayerIDToKicadLayerID( F_silk ) );
    shape->SetShape( SHAPE_T::SEGMENT );
    shape->SetStart( sp );
    shape->SetEnd( ep );
    shape->SetWidth( RetainTwoDegree( stof( "7.874" ) ) );
    m_KicadFPObj->Add( shape, ADD_MODE::APPEND, true );
    return 0;
}

bool judgeCircleIntersectsSegment(vector<SEG>& allPoly, VECTOR2I& cp ){
    for( int i = 0; i < allPoly.size(); i++ ){
        SEG ploy = allPoly[i];
        double sx = static_cast<double>( ploy.A.x );
        double sy = static_cast<double>( ploy.A.y );
        double ex = static_cast<double>( ploy.B.x );
        double ey = static_cast<double>( ploy.B.y );
        double cx = static_cast<double>( cp.x );
        double cy = static_cast<double>( cp.y );
        double radius = RetainTwoDegree( stof( "11.811" ));
        double xIntersects[2];
        double yIntersects[2];

        int count  = GeometryUVExt::GetExt().LineCircleIntersect2( sx, sy, ex, ey, cx, cy,
                              radius, xIntersects, yIntersects );
        if (count > 0) {
            std::cout << "交点数量: " << count << std::endl;
            return true;
        } 
    }
    return false;
}

void KicadFPAddFDMInfo::setReferencePosit(){

    SHAPE_POLY_SET shapeCompound = m_KicadFPObj->GetBoundingHull();

    VECTOR2I  posREFERENCE, posButton, size ;
    size.x = RetainTwoDegree( 30 );
    size.y = RetainTwoDegree( 30 );

    BOX2I  box = m_KicadFPObj->GetBoundingBox();
    posREFERENCE.x = box.GetCenter().x;
    posREFERENCE.y = box.GetCenter().y - 0.7 * box.GetHeight();
    for( int i = 1; i<40; i++ ){
        posButton.x = posREFERENCE.x;
        posButton.y = posREFERENCE.y + RetainTwoDegree( 18 );

        if( shapeCompound.Collide(posButton) ){
            posREFERENCE.y = posREFERENCE.y - RetainTwoDegree( stof( "19.685" ));
        }
        else{
            break;
        }
    }

    m_KicadFPObj->SetReference( "REF**" );
    m_KicadFPObj->Reference().SetTextPos( posREFERENCE );
    m_KicadFPObj->Reference().SetLayer( F_SilkS );
    m_KicadFPObj->Reference().SetTextSize( size );
    return;

}

int KicadFPAddFDMInfo::drawingFsilkPin1( std::string& pinID ){
 
    SHAPE_POLY_SET shapeCompound = m_KicadFPObj->GetBoundingHull();
    VECTOR2I   cp;
    VECTOR2I   ep;
    VECTOR2I   hitp;
        
    PAD* hitPad = m_KicadFPObj->FindPadByNumber( pinID.c_str() );

    if ( hitPad == nullptr ) 
        return 0;
    VECTOR2I hitPadPos = hitPad->GetPosition();

    vector<SEG> allPoly;  
    for( int poly_index = 0; poly_index < shapeCompound.OutlineCount(); ++poly_index )
    {
        const SHAPE_LINE_CHAIN& polygon = shapeCompound.Outline(poly_index);
        //  遍历多边形中的所有边
        for( int i = 0; i < polygon.SegmentCount(); ++i )
        {
            const SEG& poly = polygon.Segment(i);
            allPoly.push_back(poly); 
        }
    }

    int angle = static_cast<int>(round( hitPad->GetOrientationDegrees()) );
    // 左边
    if (angle ==90 || angle == 270 ){
        int padSizeY = (hitPad->GetSizeY())/2; // 直径
        cp.x = hitPadPos.x - padSizeY - RetainTwoDegree( stof( "7.874" )* 2);
        cp.y = hitPadPos.y;
        ep.x = hitPadPos.x - padSizeY - RetainTwoDegree( stof( "7.874" ));
        ep.y = hitPadPos.y;
    }else{
        int padSizeX = (hitPad->GetSizeX())/2; // 直径
        cp.x = hitPadPos.x - padSizeX - RetainTwoDegree( stof( "7.874" )* 2);
        cp.y = hitPadPos.y;
        ep.x = hitPadPos.x - padSizeX - RetainTwoDegree( stof( "7.874" ));
        ep.y = hitPadPos.y;
    }

    // 右边
    if ( shapeCompound.Collide(cp) || shapeCompound.Collide(ep) ){
        if (angle ==90 || angle == 270 ){
            int padSizeY = (hitPad->GetSizeY())/2; // 直径
            cp.x = hitPadPos.x + padSizeY + RetainTwoDegree( stof( "7.874" )* 2);
            cp.y = hitPadPos.y;
            ep.x = hitPadPos.x + padSizeY + RetainTwoDegree( stof( "7.874" ));
            ep.y = hitPadPos.y;
        }else{
            int padSizeX = (hitPad->GetSizeX())/2; // 直径
            cp.x = hitPadPos.x + padSizeX + RetainTwoDegree( stof( "7.874" )* 2);
            cp.y = hitPadPos.y;
            ep.x = hitPadPos.x + padSizeX + RetainTwoDegree( stof( "7.874" ));
            ep.y = hitPadPos.y;
        }
    }
    // 上边
    if ( shapeCompound.Collide(cp) || shapeCompound.Collide(ep) ){  
        //向上，垂直之方向移动。
        if (angle ==90 || angle == 270 ){
            int padSizeX = (hitPad->GetSizeX())/2; // 直径
            cp.x = hitPadPos.x;
            cp.y = hitPadPos.y - padSizeX - RetainTwoDegree( stof( "7.874" )* 2);
            ep.x = hitPadPos.x;
            ep.y = hitPadPos.y - padSizeX - RetainTwoDegree( stof( "7.874" ));
        }else{
            int padSizeY = (hitPad->GetSizeY())/2; // 直径
            cp.x = hitPadPos.x ;
            cp.y = hitPadPos.y - padSizeY - RetainTwoDegree( stof( "7.874" )* 2);
            ep.x = hitPadPos.x ;
            ep.y = hitPadPos.y - padSizeY - RetainTwoDegree( stof( "7.874" ));
        }
    }
    // 下边
    if ( shapeCompound.Collide(cp) || shapeCompound.Collide(ep) ){  
        //向下，垂直之方向移动。
        if (angle ==90 || angle == 270 ){
            int padSizeX = (hitPad->GetSizeX())/2; // 直径
            cp.x = hitPadPos.x;
            cp.y = hitPadPos.y + padSizeX + RetainTwoDegree( stof( "7.874" )* 2);
            ep.x = hitPadPos.x;
            ep.y = hitPadPos.y + padSizeX + RetainTwoDegree( stof( "7.874" ));
        }else{
            int padSizeY = (hitPad->GetSizeY())/2; // 直径
            cp.x = hitPadPos.x ;
            cp.y = hitPadPos.y + padSizeY + RetainTwoDegree( stof( "7.874" )* 2);
            ep.x = hitPadPos.x ;
            ep.y = hitPadPos.y + padSizeY + RetainTwoDegree( stof( "7.874" ));
        }
        // 左边
        if ( shapeCompound.Collide(cp) || shapeCompound.Collide(ep) ){
            if (angle ==90 || angle == 270 ){
                int padSizeY = (hitPad->GetSizeY())/2; // 直径
                cp.x = hitPadPos.x - padSizeY - RetainTwoDegree( stof( "7.874" )* 3);
                cp.y = hitPadPos.y;
                ep.x = hitPadPos.x - padSizeY - RetainTwoDegree( stof( "7.874" )* 2);
                ep.y = hitPadPos.y;
            }else{
                int padSizeX = (hitPad->GetSizeX())/2; // 直径
                cp.x = hitPadPos.x - padSizeX - RetainTwoDegree( stof( "7.874" )* 3);
                cp.y = hitPadPos.y;
                ep.x = hitPadPos.x - padSizeX - RetainTwoDegree( stof( "7.874" )* 2);
                ep.y = hitPadPos.y;
            }
        }
        // 右边
        if ( shapeCompound.Collide(cp) || shapeCompound.Collide(ep) ){
            if (angle ==90 || angle == 270 ){
                int padSizeY = (hitPad->GetSizeY())/2; // 直径
                cp.x = hitPadPos.x + padSizeY + RetainTwoDegree( stof( "7.874" )* 3);
                cp.y = hitPadPos.y;
                ep.x = hitPadPos.x + padSizeY + RetainTwoDegree( stof( "7.874" )* 2);
                ep.y = hitPadPos.y;
            }else{
                int padSizeX = (hitPad->GetSizeX())/2; // 直径
                cp.x = hitPadPos.x + padSizeX + RetainTwoDegree( stof( "7.874" )* 3);
                cp.y = hitPadPos.y;
                ep.x = hitPadPos.x + padSizeX + RetainTwoDegree( stof( "7.874" )* 2);
                ep.y = hitPadPos.y;
            }
        }
        // 上边
        if ( shapeCompound.Collide(cp) || shapeCompound.Collide(ep) ){  
            //向上，垂直之方向移动。
            if (angle ==90 || angle == 270 ){
                int padSizeX = (hitPad->GetSizeX())/2; // 直径
                cp.x = hitPadPos.x;
                cp.y = hitPadPos.y - padSizeX - RetainTwoDegree( stof( "7.874" )* 3);
                ep.x = hitPadPos.x;
                ep.y = hitPadPos.y - padSizeX - RetainTwoDegree( stof( "7.874" )* 2);
            }else{
                int padSizeY = (hitPad->GetSizeY())/2; // 直径
                cp.x = hitPadPos.x ;
                cp.y = hitPadPos.y - padSizeY - RetainTwoDegree( stof( "7.874" )* 3);
                ep.x = hitPadPos.x ;
                ep.y = hitPadPos.y - padSizeY - RetainTwoDegree( stof( "7.874" )* 2);
            }
        }
        // 下边
        if ( shapeCompound.Collide(cp) || shapeCompound.Collide(ep) ){  
            //向下，垂直之方向移动。
            if (angle ==90 || angle == 270 ){
                int padSizeX = (hitPad->GetSizeX())/2; // 直径
                cp.x = hitPadPos.x;
                cp.y = hitPadPos.y + padSizeX + RetainTwoDegree( stof( "7.874" )* 3);
                ep.x = hitPadPos.x;
                ep.y = hitPadPos.y + padSizeX + RetainTwoDegree( stof( "7.874" )* 2);
            }else{
                int padSizeY = (hitPad->GetSizeY())/2; // 直径
                cp.x = hitPadPos.x ;
                cp.y = hitPadPos.y + padSizeY + RetainTwoDegree( stof( "7.874" )* 3);
                ep.x = hitPadPos.x ;
                ep.y = hitPadPos.y + padSizeY + RetainTwoDegree( stof( "7.874" )* 2);
            }
            
            for( int k = 4; k<20; k++ ){
                // 左边
                if ( m_KicadFPObj->HitTest(cp) || m_KicadFPObj->HitTest(ep) || judgeCircleIntersectsSegment( allPoly, cp ) ){
                    if (angle ==90 || angle == 270 ){
                        int padSizeY = (hitPad->GetSizeY())/2; // 直径
                        cp.x = hitPadPos.x - padSizeY - RetainTwoDegree( stof( "7.874" )* k);
                        cp.y = hitPadPos.y;
                        ep.x = hitPadPos.x - padSizeY - RetainTwoDegree( stof( "7.874" )* (k-1));
                        ep.y = hitPadPos.y;
                    }else{
                        int padSizeX = (hitPad->GetSizeX())/2; // 直径
                        cp.x = hitPadPos.x - padSizeX - RetainTwoDegree( stof( "7.874" )* k);
                        cp.y = hitPadPos.y;
                        ep.x = hitPadPos.x - padSizeX - RetainTwoDegree( stof( "7.874" )* (k-1));
                        ep.y = hitPadPos.y;
                    }
                }
                if ( m_KicadFPObj->HitTest(cp) || m_KicadFPObj->HitTest(ep) || judgeCircleIntersectsSegment( allPoly, cp ) ){
                    if (angle ==90 || angle == 270 ){
                        int padSizeY = (hitPad->GetSizeY())/2; // 直径
                        cp.x = hitPadPos.x + padSizeY + RetainTwoDegree( stof( "7.874" )* k);
                        cp.y = hitPadPos.y;
                        ep.x = hitPadPos.x + padSizeY + RetainTwoDegree( stof( "7.874" )* (k-1));
                        ep.y = hitPadPos.y;
                    }else{
                        int padSizeX = (hitPad->GetSizeX())/2; // 直径
                        cp.x = hitPadPos.x + padSizeX + RetainTwoDegree( stof( "7.874" )* k);
                        cp.y = hitPadPos.y;
                        ep.x = hitPadPos.x + padSizeX + RetainTwoDegree( stof( "7.874" )* (k-1));
                        ep.y = hitPadPos.y;
                    }
                }
                // 上边
                if ( m_KicadFPObj->HitTest(cp) || m_KicadFPObj->HitTest(ep) || judgeCircleIntersectsSegment( allPoly, cp ) ){  
                    //向上，垂直之方向移动。
                    if (angle ==90 || angle == 270 ){
                        int padSizeX = (hitPad->GetSizeX())/2; // 直径
                        cp.x = hitPadPos.x;
                        cp.y = hitPadPos.y - padSizeX - RetainTwoDegree( stof( "7.874" )* k);
                        ep.x = hitPadPos.x;
                        ep.y = hitPadPos.y - padSizeX - RetainTwoDegree( stof( "7.874" )* (k-1));
                    }else{
                        int padSizeY = (hitPad->GetSizeY())/2; // 直径
                        cp.x = hitPadPos.x ;
                        cp.y = hitPadPos.y - padSizeY - RetainTwoDegree( stof( "7.874" )* k);
                        ep.x = hitPadPos.x ;
                        ep.y = hitPadPos.y - padSizeY - RetainTwoDegree( stof( "7.874" )* (k-1));
                    }
                }
                // 下边
                if ( m_KicadFPObj->HitTest(cp) || m_KicadFPObj->HitTest(ep) || judgeCircleIntersectsSegment( allPoly, cp ) ){  
                    //向下，垂直之方向移动。
                    if (angle ==90 || angle == 270 ){
                        int padSizeX = (hitPad->GetSizeX())/2; // 直径
                        cp.x = hitPadPos.x;
                        cp.y = hitPadPos.y + padSizeX + RetainTwoDegree( stof( "7.874" )* k);
                        ep.x = hitPadPos.x;
                        ep.y = hitPadPos.y + padSizeX + RetainTwoDegree( stof( "7.874" )* (k-1));
                    }else{
                        int padSizeY = (hitPad->GetSizeY())/2; // 直径
                        cp.x = hitPadPos.x ;
                        cp.y = hitPadPos.y + padSizeY + RetainTwoDegree( stof( "7.874" )* k);
                        ep.x = hitPadPos.x ;
                        ep.y = hitPadPos.y + padSizeY + RetainTwoDegree( stof( "7.874" )* (k-1));
                    }
                }
            }
        }


    }

    PCB_SHAPE* shape = new PCB_SHAPE( m_KicadFPObj );


    std::string F_silk = "3";
    shape->SetLayer( (PCB_LAYER_ID) LCLayerIDToKicadLayerID( F_silk ) );
    shape->SetShape( SHAPE_T::CIRCLE );
    shape->SetCenter( cp );
    shape->SetEnd( ep );
    shape->SetWidth(stof( "0" ));
    shape->SetFilled( true );
    m_KicadFPObj->Add( shape, ADD_MODE::APPEND, true );

    return 0;
}


// 解析丝印层外框大小
int KicadFPAddFDMInfo::praseSilkOutline( std::vector<ShapeInfo>& shapesInfo,std::vector<PointInfo>& pointsInfo )
{
    // float width;        // 宽度
    // float height;       // 高度
    // float x_position;   // X 坐标位置
    // float y_position;   // Y 坐标位置
    // float orientation;  // 方向
    std::vector<float> x_values;
    // std::vector<float> left_x_values;

    std::vector<float> y_values;
    // std::vector<float> below_y_values;

    for(const auto& shapeInfo : shapesInfo ){
        if(shapeInfo.orientation == 0 || shapeInfo.orientation == 180 )
        {
            x_values.push_back(shapeInfo.x_position + shapeInfo.width/2 );
            x_values.push_back(shapeInfo.x_position - shapeInfo.width/2 );

            y_values.push_back( shapeInfo.y_position + shapeInfo.height/2);
            y_values.push_back( shapeInfo.y_position - shapeInfo.height/2);

        }
        else if(shapeInfo.orientation == 90 || shapeInfo.orientation == 270 )
        {
            x_values.push_back(shapeInfo.x_position + shapeInfo.height/2 );
            x_values.push_back(shapeInfo.x_position - shapeInfo.height/2 );

            y_values.push_back( shapeInfo.y_position + shapeInfo.width/2);
            y_values.push_back( shapeInfo.y_position - shapeInfo.width/2);

        }
        else if(shapeInfo.orientation == 1 )
        {
            // "R":正方形
            x_values.push_back( shapeInfo.x_position );
            x_values.push_back( shapeInfo.x_position + shapeInfo.width );
            
            y_values.push_back( shapeInfo.y_position );
            y_values.push_back( shapeInfo.y_position - shapeInfo.height );
        }
        else if(shapeInfo.orientation == 2 )
        {
            x_values.push_back(shapeInfo.x_position + shapeInfo.width/2 );
            x_values.push_back(shapeInfo.x_position - shapeInfo.width/2 );

            y_values.push_back( shapeInfo.y_position + shapeInfo.height/2);
            y_values.push_back( shapeInfo.y_position - shapeInfo.height/2);

        } 
        else if(shapeInfo.orientation == 45 )
        {
            x_values.push_back(shapeInfo.x_position + shapeInfo.width/1.4 );
            x_values.push_back(shapeInfo.x_position - shapeInfo.width/1.4 );

            y_values.push_back( shapeInfo.y_position + shapeInfo.height/1.4 );
            y_values.push_back( shapeInfo.y_position - shapeInfo.height/1.4 );

        }
        else
        {
            x_values.push_back(shapeInfo.x_position + shapeInfo.width/1.4 );
            x_values.push_back(shapeInfo.x_position - shapeInfo.width/1.4 );

            y_values.push_back( shapeInfo.y_position + shapeInfo.height/1.4 );
            y_values.push_back( shapeInfo.y_position - shapeInfo.height/1.4 );

        }

    }
    for (const auto& pointInfo : pointsInfo) 
    {
        x_values.push_back( pointInfo.x_posi );
        y_values.push_back( pointInfo.y_posi );
    }


    // 找到 x_values 的最小值和最大值
    float max_right_x = *std::max_element(x_values.begin(), x_values.end());
    float min_left_x = *std::min_element(x_values.begin(), x_values.end());
    
    // 找到 y_values 的最小值和最大值
    float max_upper_y = *std::max_element(y_values.begin(), y_values.end());
    float min_below_y = *std::min_element(y_values.begin(), y_values.end());


    PCB_SHAPE* shape = new PCB_SHAPE( m_KicadFPObj );
    VECTOR2I   cp;
    VECTOR2I   ep;
    std::string F_silk = "3";
    
    shape->SetLayer( (PCB_LAYER_ID) LCLayerIDToKicadLayerID( F_silk ) );
        //线条矩形
    shape->SetShape( SHAPE_T::RECTANGLE );
    // cp.x = RetainTwoDegree( min_left_x - 7.874 );
    // cp.y = RetainTwoDegree(-( min_below_y - 7.874 ) );
    cp.x = RetainTwoDegree( min_left_x - 10.236);
    cp.y = RetainTwoDegree(-( min_below_y - 10.236) );

    shape->SetStart( cp );
    // ep.x = RetainTwoDegree( max_right_x + 7.874 );
    // ep.y = RetainTwoDegree(-( max_upper_y + 7.874 ) );
    ep.x = RetainTwoDegree( max_right_x + 10.236);
    ep.y = RetainTwoDegree(-( max_upper_y + 10.236) );

    shape->SetEnd( ep );
    shape->SetWidth( RetainTwoDegree( stof( "4.724" ) ) );
    
    m_KicadFPObj->Add( shape, ADD_MODE::APPEND, true );

    return 0;

}

//解析PAD
int KicadFPAddFDMInfo::parsePad( std::vector<std::string> vecStr )
{
    int resultID = 0;
    if( 13 > vecStr.size() )
    {
        return resultID;
    }

    PAD* pad = new PAD( m_KicadFPObj );

    //设置pad编号
    pad->SetNumber( vecStr[5] );

    //获取焊盘是通孔还是贴片
    string padMakeType = vecStr[4];
    int kicadPadMakeType = 0;
    if( "12" == padMakeType )
    {
        pad->SetAttribute( PAD_ATTRIB::PTH );

        pad->SetLayerSet( LSET::AllCuMask() );


        //封装数据收集孔，槽个数
        m_FPCollection->m_holeAndSlotNum++;
    }
    else
    {
        pad->SetAttribute( PAD_ATTRIB::SMD );
        kicadPadMakeType = LCLayerIDToKicadLayerID( padMakeType );

        if( 0 == LCLayerIDToKicadLayerID( padMakeType ) )
        {
            LSET copperT( 3, F_Cu, F_Paste, F_Mask );
            pad->SetLayerSet( copperT );
        }
        else if( 31 == LCLayerIDToKicadLayerID( padMakeType ) )
        {
            LSET copperT( 3, B_Cu, B_Mask, B_Paste );
            pad->SetLayerSet( copperT );
        }
    }

    //获取焊盘图元的类型
    VECTOR2I sz;
    VECTOR2I pt;
    string   strPadType = vecStr[10];
    string   strPadPolyType = vecStr[13];

    //解析焊盘（无内孔）
    if( strPadType.size() > strPadType.find( "ELLIPSE" ) )
    {
        //圆形(外框)
        // "ELLIPSE" 表示椭圆形状的 PAD，它具有两个不同的半径，用于定义椭圆的形状。
        pad->SetShape( PAD_SHAPE::CIRCLE );
        sz.x = RetainTwoDegree( stof( vecStr[11] ) );
        sz.y = RetainTwoDegree( stof( vecStr[12] ) );
        pad->SetSize( sz );
        pt.x = RetainTwoDegree( stof( vecStr[6] ) );
        pt.y = -RetainTwoDegree( stof( vecStr[7] ) ); 
        pad->SetFPRelativePosition( pt );
        pad->SetOrientation( EDA_ANGLE( stof( vecStr[8] ), DEGREES_T ) );

        m_shapesInfo.push_back(ShapeInfo(stof( vecStr[11] ), stof( vecStr[12] ), stof( vecStr[6] ), stof( vecStr[7] ), stof( vecStr[8] )));

        if( 23 < vecStr.size() )
        {
            if( CheckStringIsNumbers( vecStr[20] ) )
            {
                //阻焊扩展
                pad->SetLocalSolderMaskMargin( RetainTwoDegree( stof( vecStr[20] ) ) );
            }

            if( CheckStringIsNumbers( vecStr[21] ) )
            {
                //助焊扩展
                pad->SetLocalSolderPasteMargin( RetainTwoDegree( stof( vecStr[21] ) ) );
            }

        
        }

        if( 27 < vecStr.size() )
        {
            if( CheckStringIsNumbers( vecStr[23] ) )
            {
                switch( stoi( vecStr[23] ) )
                {
                case 0: pad->SetZoneConnection( ZONE_CONNECTION::THERMAL ); break;
                case 1: pad->SetZoneConnection( ZONE_CONNECTION::FULL ); break;
                case 2: pad->SetZoneConnection( ZONE_CONNECTION::NONE ); break;
                default: break;
                }
            }

            //发散间距
            if( CheckStringIsNumbers( vecStr[25] ) )
            {
                pad->SetThermalGap( RetainTwoDegree( stof( vecStr[25] ) ) );
            }

            //发散线宽
            if( CheckStringIsNumbers( vecStr[26] ) )
            {
                pad->SetThermalSpokeWidth( RetainTwoDegree( stof( vecStr[26] ) ) );
            }

            //发散角度
            if( CheckStringIsNumbers( vecStr[27] ) )
            {
                pad->SetThermalSpokeAngleDegrees( stof( vecStr[27] ) );
            }
        }

        m_KicadFPObj->Add( pad, ADD_MODE::APPEND );
    }
    else if( strPadType.size() > strPadType.find( "RECT" ) )
    {
        double FilletRadius = RetainTwoDegree( stof( vecStr[13] ) );
        if( 0 < FilletRadius )
        {
            //矩形(圆角矩形)
            pad->SetShape( PAD_SHAPE::ROUNDRECT );
            pad->SetRoundRectRadiusRatio( 0.01 * stof( vecStr[13] ) );
        }
        else
        {
            //矩形
            pad->SetShape( PAD_SHAPE::RECTANGLE );
        }


        sz.x = RetainTwoDegree( stof( vecStr[11]  ));
        sz.y = RetainTwoDegree( stof( vecStr[12] ) );
        pad->SetSize( sz );
        pt.x = RetainTwoDegree( stof( vecStr[6] ) );
        pt.y = RetainTwoDegree(- stof( vecStr[7] ) );
        pad->SetOrientation( EDA_ANGLE( stof( vecStr[8] ), DEGREES_T ) );
        pad->SetFPRelativePosition( pt );
        m_shapesInfo.push_back(ShapeInfo(stof( vecStr[11] ), stof( vecStr[12] ), stof( vecStr[6] ),
                                            stof( vecStr[7] ), stof( vecStr[8] )));

        if( 24 < vecStr.size() )
        {
            if( CheckStringIsNumbers( vecStr[21] ) )
            {
                //阻焊扩展
                pad->SetLocalSolderMaskMargin( RetainTwoDegree( stof( vecStr[21] ) ) );
            }

            if( CheckStringIsNumbers( vecStr[22] ) )
            {
                //助焊扩展
                pad->SetLocalSolderPasteMargin( RetainTwoDegree( stof( vecStr[22] ) ));
            }

        
        }

        if (28 < vecStr.size())
        {
            if( CheckStringIsNumbers( vecStr[25] ) )
            {
                switch( stoi( vecStr[24] ) )
                {
                case 0: pad->SetZoneConnection( ZONE_CONNECTION::THERMAL ); break;
                case 1: pad->SetZoneConnection( ZONE_CONNECTION::FULL ); break;
                case 2: pad->SetZoneConnection( ZONE_CONNECTION::NONE ); break;
                default: break;
                }
            }

            //发散间距
            if( CheckStringIsNumbers( vecStr[26] ) )
            {
                pad->SetThermalGap( RetainTwoDegree( stof( vecStr[26] ) ) );
            }

            //发散线宽
            if( CheckStringIsNumbers( vecStr[27] ) )
            {
                pad->SetThermalSpokeWidth( RetainTwoDegree( stof( vecStr[27] ) ) );
            }

            //发散角度
            if( CheckStringIsNumbers( vecStr[28] ) )
            {
                pad->SetThermalSpokeAngleDegrees( stof( vecStr[28] ) );
            }
        }

        m_KicadFPObj->Add( pad, ADD_MODE::APPEND );
    }
    else if( strPadType.size() > strPadType.find( "OVAL" ) )
    {
        //椭圆
        // "OVAL" 表示椭圆形状的 PAD，与ELLIPSE类似，但在Kicad中，OVAL用于表示近似于椭圆形状的圆形PAD。
        pad->SetShape( PAD_SHAPE::OVAL );
        sz.x = RetainTwoDegree( stof( vecStr[11] ) );
        sz.y = RetainTwoDegree( stof( vecStr[12] ) );
        pad->SetSize( sz );
        pt.x = RetainTwoDegree( stof( vecStr[6] ) );
        pt.y = RetainTwoDegree( -stof( vecStr[7] ) );
        pad->SetOrientation( EDA_ANGLE( stof( vecStr[8] ), DEGREES_T ) );
        pad->SetFPRelativePosition( pt );
        m_shapesInfo.push_back(ShapeInfo(stof( vecStr[11] ), stof( vecStr[12] ), stof( vecStr[6] ), 
                                        stof( vecStr[7] ), stof( vecStr[8] )));

        if( 23 < vecStr.size() )
        {
            if( CheckStringIsNumbers( vecStr[20] ) )
            {
                //阻焊扩展
                pad->SetLocalSolderMaskMargin( RetainTwoDegree( stof( vecStr[20] ) ) );
            }

            if( CheckStringIsNumbers( vecStr[21] ) )
            {
                //助焊扩展
                pad->SetLocalSolderPasteMargin( RetainTwoDegree( stof( vecStr[21] ) ) );
            }

        }

        if( 27 < vecStr.size() )
        {
            if( CheckStringIsNumbers( vecStr[23] ) )
            {
                switch( stoi( vecStr[23] ) )
                {
                case 0: pad->SetZoneConnection( ZONE_CONNECTION::THERMAL ); break;
                case 1: pad->SetZoneConnection( ZONE_CONNECTION::FULL ); break;
                case 2: pad->SetZoneConnection( ZONE_CONNECTION::NONE ); break;
                default: break;
                }
            }

            //发散间距
            if( CheckStringIsNumbers( vecStr[25] ) )
            {
                pad->SetThermalGap( RetainTwoDegree( stof( vecStr[25] ) ) );
            }

            //发散线宽
            if( CheckStringIsNumbers( vecStr[26] ) )
            {
                pad->SetThermalSpokeWidth( RetainTwoDegree( stof( vecStr[26] ) ) );
            }

            //发散角度
            if( CheckStringIsNumbers( vecStr[27] ) )
            {
                pad->SetThermalSpokeAngleDegrees( stof( vecStr[27] ) );
            }
        }

        m_KicadFPObj->Add( pad, ADD_MODE::APPEND );
    }
    else if( strPadType.size() > strPadType.find( "POLY" )
                && strPadPolyType.size() > strPadPolyType.find( "L" ) )
    {
        //线条多边形
        std::vector<VECTOR2I> aPoints;
        VECTOR2I endPoint;
        VECTOR2I  sp, ep,size;
        VECTOR2I sz;
        VECTOR2I pt;

        if( 13 > vecStr.size() )
        {
            //单条线段起点
            sp.x = RetainTwoDegree( stof( vecStr[11] ) );
            sp.y = RetainTwoDegree( -stof( vecStr[12] ) );
            ep.x = RetainTwoDegree( stof( vecStr[14] ) );
            ep.y = RetainTwoDegree( -stof( vecStr[15] ) );

            endPoint.x = RetainTwoDegree( stof( vecStr[14] ) );
            endPoint.y = RetainTwoDegree( -stof( vecStr[15] ) );
            // pad->AddPrimitiveSegment( sp, ep, 0 );
            aPoints.push_back( sp );
            aPoints.push_back( ep );


            m_pointsInfo.push_back(PointInfo( stof( vecStr[11] ),  stof( vecStr[12] ) ) );
            m_pointsInfo.push_back(PointInfo( stof( vecStr[14] ),  stof( vecStr[15] ) ) );

        }
        else
        {
            VECTOR2I p;
            for( int ip = 14; ip < vecStr.size() - 12; ip += 2 )
            {
                p.x = RetainTwoDegree ( stof( vecStr[ip] ) );
                p.y = RetainTwoDegree( -stof( vecStr[ip + 1] ) );


                aPoints.push_back( p );

                m_pointsInfo.push_back(PointInfo( stof( vecStr[ip] ),  stof( vecStr[ip + 1] ) ) );
            }
            pt.x = RetainTwoDegree( stof( vecStr[14] ) );
            pt.y = RetainTwoDegree( -stof( vecStr[15] ) );
        }

        pad->SetShape( PAD_SHAPE::CUSTOM );
        pad->SetAnchorPadShape( PAD_SHAPE::CIRCLE );
        pad->SetSize( { 1, 1 } );
        pad->SetFPRelativePosition( pt );
        pad->SetOrientation( EDA_ANGLE( stof( "0" ), DEGREES_T ) );


        PCB_SHAPE* shape = new PCB_SHAPE( nullptr, SHAPE_T::POLY );

        shape->SetLayer( (PCB_LAYER_ID) kicadPadMakeType );
        shape->SetPolyPoints( aPoints );
        shape->SetFilled( true );

        shape->SetWidth( 0 );

        shape->Move( -pad->GetPosition() );

        pad->AddPrimitive( shape );


        int exIndex = vecStr.size() - 16;
        if( CheckStringIsNumbers( vecStr[exIndex+12] ) )
        {
            //阻焊扩展
            pad->SetLocalSolderMaskMargin( RetainTwoDegree( stof( vecStr[exIndex + 12] ) ) );
        }

        if( CheckStringIsNumbers( vecStr[exIndex + 13] ) )
        {
            //助焊扩展
            pad->SetLocalSolderPasteMargin( RetainTwoDegree( stof( vecStr[exIndex + 13] ) ) );
        }

        if( CheckStringIsNumbers( vecStr[exIndex + 11] ) )
        {
            switch( stoi( vecStr[exIndex+11]) )
            {
            case 0: pad->SetZoneConnection( ZONE_CONNECTION::THERMAL ); break;
            case 1: pad->SetZoneConnection( ZONE_CONNECTION::FULL ); break;
            case 2: pad->SetZoneConnection( ZONE_CONNECTION::NONE ); break;
            default: break;
            }
        }

        // //发散间距
        // if( CheckStringIsNumbers( vecStr[exIndex + 13] ) )
        // {
        //     pad->SetThermalGap( RetainTwoDegree( stof( vecStr[exIndex + 13] ) ) );
        // }

        // //发散线宽
        // if( CheckStringIsNumbers( vecStr[exIndex + 15] ) )
        // {
        //     pad->SetThermalSpokeWidth( RetainTwoDegree( stof( vecStr[exIndex + 15] ) ) );
        // }

        // //发散角度
        // if( CheckStringIsNumbers( vecStr[exIndex + 14] ) )
        // {
        //     pad->SetThermalSpokeAngleDegrees( stof( vecStr[exIndex + 14] ) );
        // }

        m_KicadFPObj->Add( pad  , ADD_MODE::APPEND );
    }


    //解析焊盘（有内孔）
    string strPadDriType = vecStr[9];
    string strPadOutType = vecStr[12];
    if( strPadDriType.size() > strPadDriType.find( "ROUND" ) && 19 < vecStr.size() )
    {
        //解析pad内孔（ROUND）
        // "ROUND" 表示圆形的 PAD，它是一个圆形的孔，用于连接元件引脚或者与其它层的轨迹相连。
        pad->SetDrillShape( PAD_DRILL_SHAPE_CIRCLE );
        sz.x = RetainTwoDegree( stof( vecStr[10] ) );
        sz.y = RetainTwoDegree( stof( vecStr[11] ) );
        pad->SetDrillSize( sz );

        int MetalType = 0;
        if( "RECT" == strPadOutType )
        {
            MetalType = stof( vecStr[20] );

            if( 24 < vecStr.size() )
            {
                if( CheckStringIsNumbers( vecStr[23] ) )
                {
                    //阻焊扩展
                    pad->SetLocalSolderMaskMargin( RetainTwoDegree( stof( vecStr[23] ) ) );
                }

                if( CheckStringIsNumbers( vecStr[24] ) )
                {
                    //助焊扩展
                    pad->SetLocalSolderPasteMargin( RetainTwoDegree( stof( vecStr[24] ) ) );
                }
            }

            if( 31 < vecStr.size() )
            {
                if( CheckStringIsNumbers( vecStr[28] ) )
                {
                    switch( stoi( vecStr[28] ) )
                    {
                    case 0: pad->SetZoneConnection( ZONE_CONNECTION::THERMAL ); break;
                    case 1: pad->SetZoneConnection( ZONE_CONNECTION::FULL ); break;
                    case 2: pad->SetZoneConnection( ZONE_CONNECTION::NONE ); break;
                    default: break;
                    }
                }

                //发散间距
                if( CheckStringIsNumbers( vecStr[29] ) )
                {
                    pad->SetThermalGap( RetainTwoDegree( stof( vecStr[29] ) ) );
                }

                //发散线宽
                if( CheckStringIsNumbers( vecStr[30] ) )
                {
                    pad->SetThermalSpokeWidth( RetainTwoDegree( stof( vecStr[30] ) ) );
                }

                //发散角度
                if( CheckStringIsNumbers( vecStr[31] ) )
                {
                    pad->SetThermalSpokeAngleDegrees( stof( vecStr[31] ) );
                }
            }
        }
        else
        {
            MetalType = stof( vecStr[19] );

            if( 23 < vecStr.size() )
            {
                if( CheckStringIsNumbers( vecStr[22] ) )
                {
                    //阻焊扩展
                    pad->SetLocalSolderMaskMargin( RetainTwoDegree( stof( vecStr[22] ) ) );
                }

                if( CheckStringIsNumbers( vecStr[23] ) )
                {
                    //助焊扩展
                    pad->SetLocalSolderPasteMargin( RetainTwoDegree( stof( vecStr[23] ) ) );
                }
            }

            if (30 < vecStr.size())
            {
                if( CheckStringIsNumbers( vecStr[27] ) )
                {
                    switch( stoi( vecStr[27] ) )
                    {
                    case 0: pad->SetZoneConnection( ZONE_CONNECTION::THERMAL ); break;
                    case 1: pad->SetZoneConnection( ZONE_CONNECTION::FULL ); break;
                    case 2: pad->SetZoneConnection( ZONE_CONNECTION::NONE ); break;
                    default: break;
                    }
                }

                //发散间距
                if( CheckStringIsNumbers( vecStr[28] ) )
                {
                    pad->SetThermalGap( RetainTwoDegree( stof( vecStr[28] ) ) );
                }

                //发散线宽
                if( CheckStringIsNumbers( vecStr[29] ) )
                {
                    pad->SetThermalSpokeWidth( RetainTwoDegree( stof( vecStr[29] ) ) );
                }

                //发散角度
                if( CheckStringIsNumbers( vecStr[30] ) )
                {
                    pad->SetThermalSpokeAngleDegrees( stof( vecStr[30] ) );
                }
            }
        }
        
        if( 0 == MetalType )
        {
            //非金属化
            pad->SetAttribute( PAD_ATTRIB::NPTH );
        }


        //解析pad外框
        if( strPadOutType.size() > strPadOutType.find( "ELLIPSE" ) )
        {
            pad->SetShape( PAD_SHAPE::CIRCLE );
            sz.x = RetainTwoDegree( stof( vecStr[13] ));
            sz.y = RetainTwoDegree( stof( vecStr[14] ));
            pad->SetSize( sz );
            pt.x = RetainTwoDegree( stof( vecStr[6] ));
            pt.y = RetainTwoDegree(-stof( vecStr[7] ));
            pad->SetFPRelativePosition( pt );
            pad->SetOrientation( EDA_ANGLE( stof( vecStr[8] ), DEGREES_T ) );
            
            m_shapesInfo.push_back(ShapeInfo(stof( vecStr[13] ), stof( vecStr[14] ), stof( vecStr[6] ), 
                                        stof( vecStr[7] ), stof( vecStr[8] )));
        }
        else if( strPadOutType.size() > strPadOutType.find( "RECT" ))
        {
            //矩形(圆角矩形)
            pad->SetShape( PAD_SHAPE::ROUNDRECT );
            sz.x = RetainTwoDegree( stof( vecStr[13] ));
            sz.y = RetainTwoDegree( stof( vecStr[14] ));
            pad->SetSize( sz );
            pt.x = RetainTwoDegree( stof( vecStr[6] ));
            pt.y = RetainTwoDegree(-stof( vecStr[7] ));
            pad->SetOrientation( EDA_ANGLE( stof( vecStr[8] ), DEGREES_T ) );
            pad->SetFPRelativePosition( pt );
            pad->SetRoundRectRadiusRatio( 0.01 * stof( vecStr[15] ) );
            m_shapesInfo.push_back(ShapeInfo(stof( vecStr[13] ), stof( vecStr[14] ), stof( vecStr[6] ), 
                                        stof( vecStr[7] ), stof( vecStr[8] )));
        }
        else if( strPadOutType.size() > strPadOutType.find( "OVAL" ))
        {
            //椭圆
            pad->SetShape( PAD_SHAPE::OVAL );
            sz.x = RetainTwoDegree( stof( vecStr[13] ));
            sz.y = RetainTwoDegree( stof( vecStr[14] ));
            pad->SetSize( sz );
            pt.x = RetainTwoDegree( stof( vecStr[6] ));
            pt.y = RetainTwoDegree(-stof( vecStr[7] ));
            pad->SetOrientation( EDA_ANGLE( stof( vecStr[8] ), DEGREES_T ) );
            pad->SetFPRelativePosition( pt );
            m_shapesInfo.push_back(ShapeInfo(stof( vecStr[13] ), stof( vecStr[14] ), stof( vecStr[6] ), 
                                        stof( vecStr[7] ), stof( vecStr[8] )));
        }

        m_KicadFPObj->Add( pad, ADD_MODE::APPEND );
    }
    else if( strPadDriType.size() > strPadDriType.find( "SLOT" ) && 19 < vecStr.size() )
    {
        //解析pad内孔（SLOT）
        // "SLOT" 表示槽形的 PAD，它具有两个矩形的端点，形成一个矩形槽。
        pad->SetDrillShape( PAD_DRILL_SHAPE_OBLONG );

        int drillAngle = 0;
        if( CheckStringIsNumbers( vecStr[18] ) )
        {
            drillAngle = stoi( vecStr[18] );
        }

        double DriX = RetainTwoDegree( stof( vecStr[10] ));
        double DriY = RetainTwoDegree( stof( vecStr[11] ));
        double OutX = RetainTwoDegree( stof( vecStr[13] ));
        double OutY = RetainTwoDegree( stof( vecStr[14] ));
        if( (GeometryUVExt::GetExt().IsGreaterEqual( OutX, OutY )
                && GeometryUVExt::GetExt().IsGreaterEqual( DriX, DriY ) )
            || ( GeometryUVExt::GetExt().IsLessEqual( OutX, OutY )
                    && GeometryUVExt::GetExt().IsLessEqual( DriX, DriY ) ) )
        {
            sz.x = RetainTwoDegree( stof( vecStr[10] ));
            sz.y = RetainTwoDegree( stof( vecStr[11] ));
        }
        else
        {
            sz.x = RetainTwoDegree( stof( vecStr[11] ));
            sz.y = RetainTwoDegree( stof( vecStr[10] ));
        }
    

        pad->SetDrillSize( sz );

        int MetalType = 0;

        if( "RECT" == strPadOutType )
        {
            MetalType = stof( vecStr[20] );

            if( 24 < vecStr.size() )
            {
                if( CheckStringIsNumbers( vecStr[23] ) )
                {
                    //阻焊扩展
                    pad->SetLocalSolderMaskMargin( RetainTwoDegree( stof( vecStr[23] ) ));
                }

                if( CheckStringIsNumbers( vecStr[24] ) )
                {
                    //助焊扩展
                    pad->SetLocalSolderPasteMargin( RetainTwoDegree( stof( vecStr[24] ) ));
                }
            }

            if( 31 < vecStr.size() )
            {
                if( CheckStringIsNumbers( vecStr[28] ) )
                {
                    switch( stoi( vecStr[28] ) )
                    {
                    case 0: pad->SetZoneConnection( ZONE_CONNECTION::THERMAL ); break;
                    case 1: pad->SetZoneConnection( ZONE_CONNECTION::FULL ); break;
                    case 2: pad->SetZoneConnection( ZONE_CONNECTION::NONE ); break;
                    default: break;
                    }
                }

                //发散间距
                if( CheckStringIsNumbers( vecStr[29] ) )
                {
                    pad->SetThermalGap( RetainTwoDegree( stof( vecStr[29] ) ));
                }

                //发散线宽
                if( CheckStringIsNumbers( vecStr[30] ) )
                {
                    pad->SetThermalSpokeWidth( RetainTwoDegree( stof( vecStr[30] ) ) );
                }

                //发散角度
                if( CheckStringIsNumbers( vecStr[31] ) )
                {
                    pad->SetThermalSpokeAngleDegrees( stof( vecStr[31] ) );
                }
            }
        }
        else
        {
            MetalType = stof( vecStr[19] );

            if( 23 < vecStr.size() )
            {
                if( CheckStringIsNumbers( vecStr[22] ) )
                {
                    //阻焊扩展
                    pad->SetLocalSolderMaskMargin( RetainTwoDegree( stof( vecStr[22] ) ));
                }

                if( CheckStringIsNumbers( vecStr[23] ) )
                {
                    //助焊扩展
                    pad->SetLocalSolderPasteMargin( RetainTwoDegree( stof( vecStr[23] ) ));
                }
            }

            if( 30 < vecStr.size() )
            {
                if( CheckStringIsNumbers( vecStr[27] ) )
                {
                    switch( stoi( vecStr[27] ) )
                    {
                    case 0: pad->SetZoneConnection( ZONE_CONNECTION::THERMAL ); break;
                    case 1: pad->SetZoneConnection( ZONE_CONNECTION::FULL ); break;
                    case 2: pad->SetZoneConnection( ZONE_CONNECTION::NONE ); break;
                    default: break;
                    }
                }

                //发散间距
                if( CheckStringIsNumbers( vecStr[28] ) )
                {
                    pad->SetThermalGap( RetainTwoDegree( stof( vecStr[28] ) ));
                }

                //发散线宽
                if( CheckStringIsNumbers( vecStr[29] ) )
                {
                    pad->SetThermalSpokeWidth( RetainTwoDegree( stof( vecStr[29] )) );
                }

                //发散角度
                if( CheckStringIsNumbers( vecStr[30] ) )
                {
                    pad->SetThermalSpokeAngleDegrees( stof( vecStr[30] ) );
                }
            }
        }

        if( 0 == MetalType )
        {
            //非金属化
            pad->SetAttribute( PAD_ATTRIB::NPTH );
        }



        //解析pad外框
        if( strPadOutType.size() > strPadOutType.find( "ELLIPSE" ) )
        {
            pad->SetShape( PAD_SHAPE::CIRCLE );
            sz.x = RetainTwoDegree( stof( vecStr[13] ));
            sz.y = RetainTwoDegree( stof( vecStr[14] ));
            pad->SetSize( sz );
            pt.x = RetainTwoDegree( stof( vecStr[6] ));
            pt.y = -RetainTwoDegree( stof( vecStr[7] ));
            pad->SetOrientation( EDA_ANGLE( stof( vecStr[8] ), DEGREES_T ) );
            pad->SetFPRelativePosition( pt );
            m_shapesInfo.push_back(ShapeInfo(stof( vecStr[13] ), stof( vecStr[14] ), stof( vecStr[6] ), 
                                        stof( vecStr[7] ), stof( vecStr[8] )));
        }
        else if( strPadOutType.size() > strPadOutType.find( "RECT" ) )
        {
            //矩形(圆角矩形)
            pad->SetShape( PAD_SHAPE::ROUNDRECT );
            sz.x = RetainTwoDegree( stof( vecStr[13] ));
            sz.y = RetainTwoDegree( stof( vecStr[14] ));
            pad->SetSize( sz );
            pt.x = RetainTwoDegree( stof( vecStr[6] ));
            pt.y = -RetainTwoDegree( stof( vecStr[7] ));
            pad->SetOrientation( EDA_ANGLE( stof( vecStr[8] ), DEGREES_T ) );
            pad->SetFPRelativePosition( pt );
            pad->SetRoundRectRadiusRatio( 0.01 * stof( vecStr[15] ) );
            m_shapesInfo.push_back(ShapeInfo(stof( vecStr[13] ), stof( vecStr[14] ), stof( vecStr[6] ), 
                                        stof( vecStr[7] ), stof( vecStr[8] )));
        }
        else if( strPadOutType.size() > strPadOutType.find( "OVAL" ) )
        {
            //椭圆
            pad->SetShape( PAD_SHAPE::OVAL );
            sz.x = RetainTwoDegree( stof( vecStr[13] ));
            sz.y = RetainTwoDegree( stof( vecStr[14] ));
            pad->SetSize( sz );
            pt.x = RetainTwoDegree( stof( vecStr[6] ));
            pt.y = -RetainTwoDegree( stof( vecStr[7] ));
            pad->SetOrientation( EDA_ANGLE( stof( vecStr[8] ), DEGREES_T ) );
            pad->SetFPRelativePosition( pt );
            m_shapesInfo.push_back(ShapeInfo(stof( vecStr[13] ), stof( vecStr[14] ), stof( vecStr[6] ), 
                                        stof( vecStr[7] ), stof( vecStr[8] )));
        }

        m_KicadFPObj->Add( pad, ADD_MODE::APPEND );
    }

    return resultID;
}

//解析POLY（元件外形层）
int KicadFPAddFDMInfo::parsePOLY( std::vector<std::string> vecStr )
{
    int resultID = 0;
    // 重新解析外形框
    // return resultID;
    if( 7 > vecStr.size() )
    {
        return resultID;
    }

    PCB_SHAPE* shape = new PCB_SHAPE( m_KicadFPObj );
    VECTOR2I   cp;
    VECTOR2I   sp;
    VECTOR2I   ep;
    VECTOR2I   mp;

    string shapeLayerType = vecStr[4];
    if( 1000 > LCLayerIDToKicadLayerID( shapeLayerType ) )
    {
        // shape->SetLayer( (PCB_LAYER_ID) LCLayerIDToKicadLayerID( shapeLayerType ) );
    }
    else if( 1001 == LCLayerIDToKicadLayerID( shapeLayerType ) )
    {
        return 0;
    }
    else
    {
        return 0;
    }

    //获取POLY类型1
    string POLYType = vecStr[6];
    if( "CIRCLE" == POLYType && 9 < vecStr.size())
    {
        //线条圆
        // shape->SetShape( SHAPE_T::CIRCLE );
        // cp.x = RetainTwoDegree( stof( vecStr[7] ));
        // cp.y = -RetainTwoDegree( stof( vecStr[8] ));
        // shape->SetStart( cp );
        // ep.x = RetainTwoDegree(  stof( vecStr[7] ) + stof( vecStr[9] )  );
        // ep.y = -RetainTwoDegree( stof( vecStr[8] ) );
        // shape->SetEnd( ep );
        // shape->SetWidth( RetainTwoDegree( stof( vecStr[5] ) ) );
        // m_KicadFPObj->Add( shape, ADD_MODE::APPEND, true );

        m_shapesInfo.push_back( ShapeInfo(stof( vecStr[9] )*2 , stof( vecStr[9] )*2,  stof( vecStr[7] ) , 
                        stof( vecStr[8] ), stof( "2" ) ) );

    }
    else if( "R" == POLYType && 9 < vecStr.size() )
    {
        //线条矩形
        // shape->SetShape( SHAPE_T::RECTANGLE );
        // cp.x = RetainTwoDegree( stof( vecStr[7] ));
        // cp.y = -RetainTwoDegree( stof( vecStr[8] ));
        // shape->SetStart( cp );
        // ep.x = RetainTwoDegree( ( stof( vecStr[7] ) + stof( vecStr[9] ) ));
        // ep.y = -RetainTwoDegree( ( stof( vecStr[8] ) - stof( vecStr[10] ) ));
        // shape->SetEnd( ep );
        // shape->SetWidth( RetainTwoDegree( stof( vecStr[5] ) ));
        // m_KicadFPObj->Add( shape, ADD_MODE::APPEND, true );

        m_shapesInfo.push_back( ShapeInfo( stof( vecStr[9] ), stof( vecStr[10] ), 
            stof( vecStr[7] ) , stof( vecStr[8] ), stof( "1" ) ) );

    }

    //获取POLY类型2
    if( 9 > vecStr.size() )
    {
        return resultID;
    }
    string POLYTypeEx = vecStr[8];
    if( "L" == POLYTypeEx && 10 < vecStr.size() )
    {
        //线条多边形
        std::vector<VECTOR2I> aPoints;

        if( 13 > vecStr.size() )
        {
            //单条线段起点
            // shape->SetShape( SHAPE_T::SEGMENT );
            // sp.x = RetainTwoDegree( stof( vecStr[6] ));
            // sp.y = -RetainTwoDegree( stof( vecStr[7] ));
            // ep.x = RetainTwoDegree( stof( vecStr[9] ));
            // ep.y = -RetainTwoDegree( stof( vecStr[10] ));
            // shape->SetStart( sp );
            // shape->SetEnd( ep );

            m_pointsInfo.push_back(PointInfo( stof( vecStr[6] ),  stof( vecStr[7] ) ) );
            m_pointsInfo.push_back(PointInfo( stof( vecStr[9] ),  stof( vecStr[10] ) ) );
        }
        else
        {
            // shape->SetShape( SHAPE_T::POLY );
            VECTOR2I p;
            for( int ip = 9; ip < vecStr.size() - 2; ip += 2 )
            {
                if( !CheckStringIsNumbers( vecStr[ip] ))
                {
                    break;
                }
                // p.x = RetainTwoDegree( ( stof( vecStr[ip] ) ));
                // p.y = -RetainTwoDegree( ( stof( vecStr[ip + 1] )) );
                // aPoints.push_back( p );

                m_pointsInfo.push_back(PointInfo( stof( vecStr[ip] ),  stof( vecStr[ip + 1] ) ) );
            }
            // shape->SetPolyPoints( aPoints );
        }


        // shape->SetWidth( RetainTwoDegree( stof( vecStr[5] ) ));

        // m_KicadFPObj->Add( shape, ADD_MODE::APPEND, true );
    }
    else if( "ARC" == POLYTypeEx && 11 < vecStr.size() )
    {
        
        //线条两点圆弧
        m_pointsInfo.push_back(PointInfo( stof( vecStr[6] ),  stof( vecStr[7] ) ) );
        m_pointsInfo.push_back(PointInfo( stof( vecStr[10] ),  stof( vecStr[11] ) ) );


        // GetKicadArcAngle( vecStr ,shape);
        // m_KicadFPObj->Add( shape, ADD_MODE::APPEND, true );
    }
    else if( "CARC" == POLYTypeEx && 11 < vecStr.size() )
    {

        m_pointsInfo.push_back(PointInfo( stof( vecStr[6] ),  stof( vecStr[7] ) ) );
        m_pointsInfo.push_back(PointInfo( stof( vecStr[10] ),  stof( vecStr[11] ) ) );

        //线条中心圆弧
        // GetKicadArcAngle( vecStr, shape );
        // m_KicadFPObj->Add( shape, ADD_MODE::APPEND, true );
    }


    // //收集元件外形层尺寸
    // if( "48" == shapeLayerType )
    // {
    //     m_FPCollection->m_sizeComOutLineL =
    //             shape->GetBoundingBox().GetWidth() / (double) LCFPExpend;
    //     m_FPCollection->m_sizeComOutLineW =
    //             shape->GetBoundingBox().GetHeight() / (double) LCFPExpend;
    //     m_KicadFPObj->Remove( shape );
    // }

    return resultID;
}

//解析FILL（元件标识层，引脚焊接层，引脚悬空层）
int KicadFPAddFDMInfo::parseFILL( std::vector<std::string> vecStr )
{
    int resultID = 0;
    if( 10 > vecStr.size() )
    {
        return resultID;
    }
    
    //立创专业版EDA封装中 填充区域和挖槽区域同属于FILL，区别标识就是按层ID区分，
    //填充区域 放在顶层（层id：1），挖槽区域放在多层（层id：12），在封装中填充区域和
    //禁止区域常出现在PCB板内，几乎不会在封装中出现，因此禁止区域和填充区域在此不
    //做转换处理，对于立创封装FILL标识的，目前只转换挖槽区域（FILL+层id（12）确认）
    PCB_SHAPE* shape = new PCB_SHAPE( m_KicadFPObj );
    VECTOR2I   cp;
    VECTOR2I   sp;
    VECTOR2I   ep;

    string shapeLayerType = vecStr[4];

    if( "12" == shapeLayerType )
    {
        shape->SetLayerSet( Edge_Cuts );
    }
    else if( "49" == shapeLayerType )
    {
        return 0;
    }
    else if(  "3" == shapeLayerType )
    {
        return 0;
    }
    else
    {
        if( 1000 != LCLayerIDToKicadLayerID( shapeLayerType ) )
        {
            shape->SetLayer( (PCB_LAYER_ID) LCLayerIDToKicadLayerID( shapeLayerType ) );
        }
        else
        {
            return 0;
        }

    }

    //获取FILL类型1
    string FilType = vecStr[7];
    if( "CIRCLE" == FilType && 10 < vecStr.size() )
    {
        //填充圆
        shape->SetShape( SHAPE_T::CIRCLE );
        cp.x = RetainTwoDegree( stof( vecStr[8] ));
        cp.y = -RetainTwoDegree( stof( vecStr[9] ));
        shape->SetCenter( cp );
        ep.x = RetainTwoDegree( ( stof( vecStr[8] ) + stof( vecStr[10] ) ));
        ep.y = -RetainTwoDegree( stof( vecStr[9] ));
        shape->SetEnd( ep );
        shape->SetFilled( true );

        m_shapesInfo.push_back( ShapeInfo(stof( vecStr[10] )*2 , stof( vecStr[10] )*2,  stof( vecStr[8] ) , 
                        stof( vecStr[9] ), stof( "2" ) ) );


        m_KicadFPObj->Add( shape, ADD_MODE::APPEND, true );
    }
    else if( "R" == FilType && 11 < vecStr.size() )
    {
        //填充矩形
        shape->SetShape( SHAPE_T::RECTANGLE );
        cp.x = RetainTwoDegree( stof( vecStr[8] ));
        cp.y = -RetainTwoDegree( stof( vecStr[9] ));
        shape->SetStart( cp );
        ep.x = RetainTwoDegree( ( stof( vecStr[8] ) + stof( vecStr[10] ) ));
        ep.y = -RetainTwoDegree( ( stof( vecStr[9] ) - stof( vecStr[11] ) ));
        shape->SetEnd( ep );

        shape->SetFilled( true );

        m_shapesInfo.push_back( ShapeInfo( stof( vecStr[10] ), stof( vecStr[11] ), 
                        stof( vecStr[8] ) , stof( vecStr[9] ), stof( "1" ) ) );

        m_KicadFPObj->Add( shape, ADD_MODE::APPEND, true );
    }

    //获取FILL类型2
    string FilTypeEx = vecStr[9];
    if( "L" == FilTypeEx)
    {
        //填充多边形
        std::vector<VECTOR2I> aPoints;
        shape->SetShape( SHAPE_T::POLY );
        VECTOR2I p;
        for( int ip = 10; ip < vecStr.size() - 2; ip += 2 )
        {
            if( !CheckStringIsNumbers( vecStr[ip] ) )
            {
                return 0;
            }
            p.x = RetainTwoDegree( ( stof( vecStr[ip] ) ));
            p.y = -RetainTwoDegree( ( stof( vecStr[ip + 1] )) );
            aPoints.push_back( p );

            m_pointsInfo.push_back(PointInfo( stof( vecStr[ip] ),  stof( vecStr[ip + 1] ) ) );
        }

        shape->SetPolyPoints( aPoints );
        shape->SetFilled( true );
        m_KicadFPObj->Add( shape, ADD_MODE::APPEND, true );
    }


    //收集元件标识层信息
    if( "49" == shapeLayerType )
    {
        m_FPCollection->m_flagPX = shape->GetPosition().x / (double) LCFPExpend;
        m_FPCollection->m_flagPY = shape->GetPosition().y / (double) LCFPExpend;
        m_FPCollection->m_isFlag = true;
        m_KicadFPObj->Remove( shape );
    }

    return resultID;
}

//解析STRING
int KicadFPAddFDMInfo::parseSTRING( std::vector<std::string> vecStr )
{
    int resultID = 0;
    return resultID;
    if( 9 > vecStr.size() )
    {
        return resultID;
    }
    VECTOR2I  cp, size;
    PCB_TEXT* text = new PCB_TEXT( m_KicadFPObj );

    string textLayerType = vecStr[3];
    if( 1000 != LCLayerIDToKicadLayerID( textLayerType ) )
    {
        text->SetLayer( (PCB_LAYER_ID) LCLayerIDToKicadLayerID( textLayerType ) );
    }
    else
    {
        return 0;
    }

    cp.x = RetainTwoDegree( ( stof( vecStr[4] ) + 5 ));
    cp.y = -RetainTwoDegree( ( stof( vecStr[5] ) + 25 ));
    text->SetTextPos( cp );
    text->SetText( vecStr[6] );
    size.x = RetainTwoDegree( ( stof( vecStr[8] ) ));
    size.y = RetainTwoDegree( ( stof( vecStr[8] ) ));
    text->SetTextSize( size );
    text->SetTextThickness( 0.3 );

    
    int strSize = vecStr[6].size();
    // m_pointsInfo.push_back(PointInfo( ( stof(vecStr[4]) + 5 - stof(vecStr[8])/2*strSize ) , ( stof(vecStr[5]) + 25 - stof(vecStr[8])/2 ) ) );
    // m_pointsInfo.push_back(PointInfo( ( stof(vecStr[4]) + 5 + stof(vecStr[8])/2*strSize ), ( stof(vecStr[5]) + 25 + stof(vecStr[8])/1.5 ) ) );
    m_KicadFPObj->Add( text, ADD_MODE::APPEND, true );
    return resultID;
}

//解析REGION
int KicadFPAddFDMInfo::parseREGION( std::vector<std::string> vecStr )
{
    //针对立创专业版封装中的禁止区域不做转换处理
    return 0;

    int resultID = 0;
    if( 4 > vecStr.size() )
    {
        return resultID;
    }

    ZONE*    zone = new ZONE( m_KicadFPObj );
    VECTOR2I cp;
    VECTOR2I sp;
    VECTOR2I ep;

    string zoneLayerType = vecStr[3];
    if( 1000 != LCLayerIDToKicadLayerID( zoneLayerType ) )
    {
        zone->SetLayer( (PCB_LAYER_ID) LCLayerIDToKicadLayerID( zoneLayerType ) );
    }
    else
    {
        return 0;
    }

    //获取FILL类型1
    string RegionType = "";
    int    RegionTypeIndex = 0;
    for( int num = 0; num < vecStr.size(); num++ )
    {
        if( "CIRCLE" == vecStr[num] )
        {
            RegionType = "CIRCLE";
            RegionTypeIndex = num;
        }
        else if( "R" == vecStr[num] )
        {
            RegionType = "R";
            RegionTypeIndex = num;
        }
        else if( "L" == vecStr[num] )
        {
            RegionType = "L";
            RegionTypeIndex = num;
        }
    }


    if( "CIRCLE" == RegionType )
    {
        //填充圆
        PCB_SHAPE* shape = new PCB_SHAPE( m_KicadFPObj );
        shape->SetShape( SHAPE_T::CIRCLE );
        cp.x = RetainTwoDegree( stof( vecStr[RegionTypeIndex + 1] ) );
        cp.y = RetainTwoDegree( stof( vecStr[RegionTypeIndex + 2] ) );
        shape->SetCenter( cp );
        ep.x = LCFPExpendNum
                * ( stof( vecStr[RegionTypeIndex + 1] ) + stof( vecStr[RegionTypeIndex + 3] ) );
        ep.y = RetainTwoDegree( stof( vecStr[RegionTypeIndex + 2] ) );
        shape->SetEnd( ep );
        shape->SetFilled( true );

        m_shapesInfo.push_back( ShapeInfo(stof( vecStr[RegionTypeIndex + 3] )*2, stof( vecStr[RegionTypeIndex + 3] )*2, 
                        stof( vecStr[RegionTypeIndex + 1] ) , stof( vecStr[RegionTypeIndex + 2] ), stof( "2" ) ) );

        m_KicadFPObj->Add( shape, ADD_MODE::APPEND, true );
    }
    else if( "R" == RegionType )
    {
        //填充矩形
        PCB_SHAPE* shape = new PCB_SHAPE( m_KicadFPObj );
        shape->SetShape( SHAPE_T::RECTANGLE );
        cp.x = RetainTwoDegree( stof( vecStr[RegionTypeIndex + 1] ) );
        cp.y = RetainTwoDegree( stof( vecStr[RegionTypeIndex + 2] ) );
        shape->SetStart( cp );
        ep.x = LCFPExpendNum
                * ( stof( vecStr[RegionTypeIndex + 1] ) + stof( vecStr[RegionTypeIndex + 3] ) );
        ep.y = LCFPExpendNum
                * ( stof( vecStr[RegionTypeIndex + 2] ) - stof( vecStr[RegionTypeIndex + 4] ) );
        shape->SetEnd( ep );
        shape->SetFilled( true );

        m_shapesInfo.push_back( ShapeInfo(stof( vecStr[RegionTypeIndex + 3] ), stof( vecStr[RegionTypeIndex + 4] ), 
                        stof( vecStr[RegionTypeIndex + 1] ) , stof( vecStr[RegionTypeIndex + 2] ), stof( "1" ) ) );


        m_KicadFPObj->Add( shape, ADD_MODE::APPEND, true );
    }

    //获取FILL类型2
    if( "L" == RegionType )
    {
        std::vector<VECTOR2I> aPoints;
        zone->SetZoneName( vecStr[1] );
        zone->SetFillMode( ZONE_FILL_MODE::HATCH_PATTERN );
        zone->SetThermalReliefGap( 0.5 );
        zone->SetIsRuleArea( true );

        zone->SetDoNotAllowCopperPour( false );
        zone->SetDoNotAllowVias( false );
        zone->SetDoNotAllowTracks( false );
        zone->SetDoNotAllowPads( false );
        zone->SetDoNotAllowFootprints( false );

        int typeNum = RegionTypeIndex - 7;
        switch( typeNum )
        {
        case 1:
            if( CheckStringIsNumbers( vecStr[5] ) )
            {
                switch( stoi( vecStr[5] ) )
                {
                case 2: zone->SetDoNotAllowFootprints( true ); break;
                case 5: zone->SetDoNotAllowTracks( true ); break;
                case 6: break;
                case 7: zone->SetDoNotAllowCopperPour( true ); break;
                case 8: break;

                default: break;
                }
            }
            break;
        case 2:
            if( CheckStringIsNumbers( vecStr[5] ) )
            {
                switch( stoi( vecStr[5] ) )
                {
                case 2: zone->SetDoNotAllowFootprints( true ); break;
                case 5: zone->SetDoNotAllowTracks( true ); break;
                case 6: break;
                case 7: zone->SetDoNotAllowCopperPour( true ); break;
                case 8: break;

                default: break;
                }
            }
            if( CheckStringIsNumbers( vecStr[6] ) )
            {
                switch( stoi( vecStr[6] ) )
                {
                case 2: zone->SetDoNotAllowFootprints( true ); break;
                case 5: zone->SetDoNotAllowTracks( true ); break;
                case 6: break;
                case 7: zone->SetDoNotAllowCopperPour( true ); break;
                case 8: break;

                default: break;
                }
            }
            break;
        case 3:
            break;
            if( CheckStringIsNumbers( vecStr[5] ) )
            {
                switch( stoi( vecStr[5] ) )
                {
                case 2: zone->SetDoNotAllowFootprints( true ); break;
                case 5: zone->SetDoNotAllowTracks( true ); break;
                case 6: break;
                case 7: zone->SetDoNotAllowCopperPour( true ); break;
                case 8: break;

                default: break;
                }
            }
            if( CheckStringIsNumbers( vecStr[6] ) )
            {
                switch( stoi( vecStr[6] ) )
                {
                case 2: zone->SetDoNotAllowFootprints( true ); break;
                case 5: zone->SetDoNotAllowTracks( true ); break;
                case 6: break;
                case 7: zone->SetDoNotAllowCopperPour( true ); break;
                case 8: break;

                default: break;
                }
            }
            if( CheckStringIsNumbers( vecStr[7] ) )
            {
                switch( stoi( vecStr[7] ) )
                {
                case 2: zone->SetDoNotAllowFootprints( true ); break;
                case 5: zone->SetDoNotAllowTracks( true ); break;
                case 6: break;
                case 7: zone->SetDoNotAllowCopperPour( true ); break;
                case 8: break;

                default: break;
                }
            }
        case 4:
            break;
            if( CheckStringIsNumbers( vecStr[5] ) )
            {
                switch( stoi( vecStr[5] ) )
                {
                case 2: zone->SetDoNotAllowFootprints( true ); break;
                case 5: zone->SetDoNotAllowTracks( true ); break;
                case 6: break;
                case 7: zone->SetDoNotAllowCopperPour( true ); break;
                case 8: break;

                default: break;
                }
            }
            if( CheckStringIsNumbers( vecStr[6] ) )
            {
                switch( stoi( vecStr[6] ) )
                {
                case 2: zone->SetDoNotAllowFootprints( true ); break;
                case 5: zone->SetDoNotAllowTracks( true ); break;
                case 6: break;
                case 7: zone->SetDoNotAllowCopperPour( true ); break;
                case 8: break;

                default: break;
                }
            }
            if( CheckStringIsNumbers( vecStr[7] ) )
            {
                switch( stoi( vecStr[7] ) )
                {
                case 2: zone->SetDoNotAllowFootprints( true ); break;
                case 5: zone->SetDoNotAllowTracks( true ); break;
                case 6: break;
                case 7: zone->SetDoNotAllowCopperPour( true ); break;
                case 8: break;

                default: break;
                }
            }
            if( CheckStringIsNumbers( vecStr[8] ) )
            {
                switch( stoi( vecStr[8] ) )
                {
                case 2: zone->SetDoNotAllowFootprints( true ); break;
                case 5: zone->SetDoNotAllowTracks( true ); break;
                case 6: break;
                case 7: zone->SetDoNotAllowCopperPour( true ); break;
                case 8: break;

                default: break;
                }
            }
        case 5:
            break;
            if( CheckStringIsNumbers( vecStr[5] ) )
            {
                switch( stoi( vecStr[5] ) )
                {
                case 2: zone->SetDoNotAllowFootprints( true ); break;
                case 5: zone->SetDoNotAllowTracks( true ); break;
                case 6: break;
                case 7: zone->SetDoNotAllowCopperPour( true ); break;
                case 8: break;

                default: break;
                }
            }
            if( CheckStringIsNumbers( vecStr[6] ) )
            {
                switch( stoi( vecStr[6] ) )
                {
                case 2: zone->SetDoNotAllowFootprints( true ); break;
                case 5: zone->SetDoNotAllowTracks( true ); break;
                case 6: break;
                case 7: zone->SetDoNotAllowCopperPour( true ); break;
                case 8: break;

                default: break;
                }
            }
            if( CheckStringIsNumbers( vecStr[7] ) )
            {
                switch( stoi( vecStr[7] ) )
                {
                case 2: zone->SetDoNotAllowFootprints( true ); break;
                case 5: zone->SetDoNotAllowTracks( true ); break;
                case 6: break;
                case 7: zone->SetDoNotAllowCopperPour( true ); break;
                case 8: break;

                default: break;
                }
            }
            if( CheckStringIsNumbers( vecStr[8] ) )
            {
                switch( stoi( vecStr[8] ) )
                {
                case 2: zone->SetDoNotAllowFootprints( true ); break;
                case 5: zone->SetDoNotAllowTracks( true ); break;
                case 6: break;
                case 7: zone->SetDoNotAllowCopperPour( true ); break;
                case 8: break;

                default: break;
                }
            }
            if( CheckStringIsNumbers( vecStr[9] ) )
            {
                switch( stoi( vecStr[9] ) )
                {
                case 2: zone->SetDoNotAllowFootprints( true ); break;
                case 5: zone->SetDoNotAllowTracks( true ); break;
                case 6: break;
                case 7: zone->SetDoNotAllowCopperPour( true ); break;
                case 8: break;

                default: break;
                }
            }
        default: break;
        }

        VECTOR2I p;
        for( int ip = RegionTypeIndex + 1; ip < vecStr.size() - 2; ip += 2 )
        {
            p.x = RetainTwoDegree( ( stof( vecStr[ip] ) ));
            p.y = RetainTwoDegree( ( stof( vecStr[ip + 1] )) );
            aPoints.push_back( p );

            m_pointsInfo.push_back(PointInfo( stof( vecStr[ip] ),  stof( vecStr[ip + 1] ) ) );
        }

        zone->AddPolygon( aPoints );

        m_KicadFPObj->Add( zone, ADD_MODE::APPEND, true );
    }


    return resultID;
}

//解析ATTR
int KicadFPAddFDMInfo::parseATTR( std::vector<std::string> vecStr )
{
    int resultID = 0;
    if( 9 > vecStr.size() )
    {
        return resultID;
    }

    if( "Footprint" == vecStr[7] )
    {
        VECTOR2I  posUser, posVALUE, posREFERENCE, size;
        BOX2I     box = m_KicadFPObj->GetBoundingBox();
        PCB_TEXT* textUser = new PCB_TEXT( m_KicadFPObj );
        size.x = RetainTwoDegree( 30 );
        size.y = RetainTwoDegree( 30 );

        textUser->SetText( "${REFERENCE}" );
        posUser.x = box.GetCenter().x;
        posUser.y = box.GetCenter().y;
        textUser->SetTextPos( posUser );
        textUser->SetLayer( F_Fab );
        textUser->SetTextSize( size );
        m_KicadFPObj->Add( textUser, ADD_MODE::APPEND, true );


        m_KicadFPObj->SetValue( vecStr[8] );

        posVALUE.x = box.GetCenter().x;
        posVALUE.y = box.GetCenter().y + 0.7 * box.GetHeight();
        posREFERENCE.x = box.GetCenter().x;
        posREFERENCE.y = box.GetCenter().y - 0.7 * box.GetHeight();

        //
        m_KicadFPObj->Value().SetTextPos( posVALUE );
        m_KicadFPObj->Value().SetLayer(F_Fab);
        m_KicadFPObj->Value().SetTextSize( size );

        // VECTOR2I  posREFERENCE, size;
        // size.x = RetainTwoDegree( 30 );
        // size.y = RetainTwoDegree( 30 );
        // m_KicadFPObj->SetReference( "REF**" );
        // m_KicadFPObj->Reference().SetTextPos( posREFERENCE );
        // m_KicadFPObj->Reference().SetLayer( F_SilkS );
        // m_KicadFPObj->Reference().SetTextSize( size );
        

        
    }

    return resultID;
}

//判断字符串是否全为数字
bool KicadFPAddFDMInfo::CheckStringIsNumbers( std::string str )
{
    bool result = false;
    result = ( "" == str ) ? false : true;

    for( int i = 0; i < str.size(); i++ )
    {
        //_与.  ？
        if( ( str[i] >= '0' && str[i] <= '9' ) || '-' == str[i] || '.' == str[i] )
        {
            continue;
        }
        else
        {
            return false;
        }
    }

    return result;
}

//字符串分割
std::vector<std::string> KicadFPAddFDMInfo::Split( std::string strContext,
                                                        std::string StrDelimiter )
{
    vector<string> vecResult;
    if( strContext.empty() )
    {
        return vecResult;
    }

    if( StrDelimiter.empty() )
    {
        vecResult.push_back( strContext );
        return vecResult;
    }

    strContext += StrDelimiter;

    int iSize = strContext.size();
    for( int i = 0; i < iSize; i++ )
    {
        int iPos = strContext.find( StrDelimiter, i );
        if( iPos < iSize )
        {
            string strElement = strContext.substr( i, iPos - i );
            vecResult.push_back( strElement );
            i = iPos + StrDelimiter.size() - 1;
        }
    }

    return vecResult;
}

//去除vector容器内单个元素的前后双引号
void KicadFPAddFDMInfo::vecStrAnalyse( std::vector<std::string> vecStr )
{
    for( int i = 0; i < vecStr.size(); i++ )
    {
        if( vecStr[i].size() > vecStr[i].find( "\"" ) )
        {
            string str = vecStr[i].substr( 1, vecStr[i].size() - 2 );
            vecStr[i] = vecStr[i].substr( 1, vecStr[i].size() - 2 );
        }
    }
}

// 字符串批量替换(strInput:输入的文本,dStr:需要被替换的文本，replaceStr：替换的文本，返回：替换好的文本)
std::string KicadFPAddFDMInfo::spp( std::string strInput, std::string dStr,std::string replaceStr )
{
    int oldPos = 0;
    while( strInput.find( dStr, oldPos ) != -1 ) //在未被替换的文本中寻找目标文本
    {
        int start = strInput.find( dStr, oldPos ); //找到目标文本的起始下标
        strInput.replace( start, dStr.size(), replaceStr );
        oldPos = start + replaceStr.size(); //记录未替换文本的起始下标
    }
    return strInput;
}

//立创专业版EDA封装层ID转换为KiCad的封装层ID
int KicadFPAddFDMInfo::LCLayerIDToKicadLayerID( std::string layerID )
{
    int resultID = 1000;

    if( CheckStringIsNumbers( layerID ) )
    {
        int layID = stoi( layerID );
        switch( layID )
        {
        case 1: resultID = 0; break;//顶层铜
        case 2: resultID = 31; break;//底层铜
        case 3: resultID = 37; break;//顶层丝印层
        case 4: resultID = 36; break;//底层丝印层
        case 5: resultID = 39; break;//顶层阻焊层
        case 6: resultID = 38; break;//底层阻焊层
        case 7: resultID = 35; break;//顶层锡膏层（顶层助焊层）
        case 8: resultID = 34; break;//底层锡膏层（底层助焊层）
        case 12: resultID = 0; break;//立创（多层）
        case 48: resultID = 1001; break; //立创（元件外形层）
        default: break;
        }
    }

    return resultID;
}

//获取Kicad 圆弧角度
int KicadFPAddFDMInfo::GetKicadArcAngle( std::vector<std::string> vecStr,PCB_SHAPE* shape )
{
    // 中间点，起点，终点
    VECTOR2I cp;
    VECTOR2I sp;
    VECTOR2I ep;
    double   cx[2];
    double   cy[2];
    double   angle = stof( vecStr[9] );
    GeometryUVExt::GetExt().GetArcCentersBySweepAngle( stof( vecStr[6] ), -stof( vecStr[7] ),
                                                stof( vecStr[10] ), -stof( vecStr[11] ),
                                                        abs( angle ), cx, cy );

    cp.x = RetainTwoDegree( cx[0] );
    cp.y = RetainTwoDegree( cy[0] );
    shape->SetShape( SHAPE_T::ARC );
    sp.x = RetainTwoDegree( stof( vecStr[6] ) );
    sp.y = -RetainTwoDegree( stof( vecStr[7] ));
    ep.x = RetainTwoDegree( stof( vecStr[10] ));
    ep.y = -RetainTwoDegree( stof( vecStr[11] ));
    shape->SetWidth( RetainTwoDegree( stof( vecStr[5] ) ));
    shape->SetCenter( cp );
    if( 0 < stof( vecStr[9] ) )
    {
        shape->SetStart( ep );
        shape->SetEnd( sp );
    }
    else
    {
        shape->SetStart( sp );
        shape->SetEnd( ep );
    }

    double   ArcAngle = shape->GetArcAngle().AsDegrees();
    if(fabs(fabs( ArcAngle ) - fabs( angle )) < 1)
    {
        return 1;
    }
    else
    {
        cp.x = RetainTwoDegree( cx[1] );
        cp.y = RetainTwoDegree( cy[1]);
        shape->SetShape( SHAPE_T::ARC );
        sp.x = RetainTwoDegree( stof( vecStr[6] ));
        sp.y = -RetainTwoDegree( stof( vecStr[7] ));
        ep.x = RetainTwoDegree( stof( vecStr[10] ));
        ep.y = -RetainTwoDegree( stof( vecStr[11] ));
        shape->SetWidth( RetainTwoDegree( stof( vecStr[5] ) ));
        shape->SetCenter( cp );
        if( 0 < stof( vecStr[9] ) )
        {
            shape->SetStart( ep );
            shape->SetEnd( sp );
        }
        else
        {
            shape->SetStart( sp );
            shape->SetEnd( ep );
        }
    }

    return 1;
}

//解析获取收集数据
int KicadFPAddFDMInfo::GetCollectInfor()
{
    if (nullptr == m_KicadFPObj)
    {
        return 0;
    }

    //获取焊盘层外框矩形作为引脚外包矩形
    BOX2I FPPinBox = m_KicadFPObj->GetBoundingBox( false, false );
    m_FPCollection->m_sizeComPinL = FPPinBox.GetWidth() / (double) LCFPExpend;
    m_FPCollection->m_sizeComPinW = FPPinBox.GetHeight() / (double) LCFPExpend;

    //总引脚数
    m_FPCollection->m_tatalPinNum = m_KicadFPObj->Pads().size();

    return 1;
}


