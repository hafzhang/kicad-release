
#ifndef KicadFPAddFDMInfo_H
#define KicadFPAddFDMInfo_H

#include <wx/dir.h>      // 包含 wxDir 头文件
#include <wx/string.h>   // 包含 wxString 头文件
#include <wx/arrstr.h>   // 包含 wxArrayString 头文件
#include <math/vector2d.h>
#include <geometry/shape_poly_set.h>

class BOARD;
class FOOTPRINT;
class PCB_SHAPE;
class LCFPInforCollection;
class LCDeviceInfor;

struct ShapeInfo;
struct PointInfo;
class KicadFPAddFDMInfo
{
    //立创专业版EDA封装转Kicad封装
public:
    KicadFPAddFDMInfo(BOARD *board);
    ~KicadFPAddFDMInfo();

public:
    //解析立创EDA专业版封装文件，并存储数据至Kicad数据结构
    int importLCFP( std::string strInFileFullPath ,int fileType=0);

    //导出Kicad封装文件
    int exportKicadFP( std::string strOutFileFullPath );

    //

private:
    //按行读取立创封装文件
    int readLCFPFileByLine( std::string strInFileFullPath, std::vector<std::string>& vecLCFPLines );

    int praseDFMJsonData();
        
    wxArrayString GetAllFilesInDir1(const wxString& strDir);

    int drawingFsilkPolarity( std::string& pinID );

    int drawingFsilkOutline( SHAPE_POLY_SET outline );

    int drawingSegment(const VECTOR2I& sp, const VECTOR2I& ep);

    int drawingFsilkPin1( std::string& pinID );
    
    void setReferencePosit();

    int drawingFFabOutline( std::vector<std::string>& pointsInfo );

    //解析行数据几何
    int parseLines( std::vector<std::string> vecLCFPLines );

    // 解析丝印层外框大小
    int praseSilkOutline( std::vector<ShapeInfo>& shapesInfo , std::vector<PointInfo>& pointsInfo );
    //解析PAD
    int parsePad( std::vector<std::string> vecStr );

    //解析POLY
    int parsePOLY( std::vector<std::string> vecStr );

    //解析FILL
    int parseFILL( std::vector<std::string> vecStr );

    //解析STRING
    int parseSTRING( std::vector<std::string> vecStr );

    //解析REGION
    int parseREGION( std::vector<std::string> vecStr );

    //解析ATTR
    int parseATTR( std::vector<std::string> vecStr );

    //字符串分割
    std::vector<std::string> Split( std::string strContext, std::string StrDelimiter );
    
    //判断字符串是否全为数字
    bool CheckStringIsNumbers( std::string str );
    
    //去除vector容器内单个元素的前后双引号
    void vecStrAnalyse( std::vector<std::string> vecStr);

    //字符串批量替换(strInput:输入的文本,dStr:需要被替换的文本，replaceStr：替换的文本，返回：替换好的文本)
    std::string spp( std::string strInput, std::string dStr, std::string replaceStr );

    //立创专业版EDA封装层ID转换为KiCad的封装层ID
    int LCLayerIDToKicadLayerID( std::string layerID );

    //获取Kicad 圆弧角度
    int GetKicadArcAngle( std::vector<std::string> vecStr, PCB_SHAPE* shape );

    //解析获取收集数据
    int GetCollectInfor();

public:
    //单个立创EDA专业版封装文件路径
    std::string m_LCFPSingleFullPath;
    std::string footprintName;

    //Kicad封装结构指针
    FOOTPRINT* m_KicadFPObj;

    //Board
    BOARD* m_board;

    //封装信息收集
    LCFPInforCollection* m_FPCollection;

    //器件信息收集
    LCDeviceInfor* m_DeviceInfor;
    
    //PAD外形信息
    std::vector<ShapeInfo> m_shapesInfo; 
    std::vector<PointInfo> m_pointsInfo; 

    std::vector<float> x_extreme_values;
    std::vector<float> y_extreme_values;

    std::string pinID ;
    std::string polarity ;
private:

};

#define DFMExpend 1000000

// 只取整数部分的头两位
inline static float DFMRetainTwoDegree( float digits )
{
    float result = DFMExpend * digits;
    
    // 将浮点数乘以100并四舍五入
    float firstTwoDigits = roundf(result/ 1000 ) * 1000;


    return firstTwoDigits ;

}

#endif