/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2022 Mark Roszko <mark.roszko@gmail.com>
 * Copyright (C) 1992-2022 KiCad Developers, see AUTHORS.txt for contributors.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef JOB_H
#define JOB_H

#include <kicommon.h>
#include <map>
#include <wx/string.h>

/**
 * An simple container class that lets us dispatch output jobs to kifaces
 */
class KICOMMON_API JOB
{
public:
    JOB( const std::string& aType, bool aIsCli );

    virtual ~JOB() {}

    // GetType(): 返回作业的类型，即成员变量 m_type 的值。
    // IsCli(): 返回一个布尔值，指示对象是否在命令行环境下使用，即成员变量 m_isCli 的值。
    // GetVarOverrides(): 返回一个 std::map<wxString, wxString> 类型的引用，表示作业的变量重写。该函数返回成员变量 m_varOverrides 的引用。
    // SetVarOverrides(): 接受一个 std::map<wxString, wxString> 类型的参数 aVarOverrides，用于设置作业的变量重写。该函数将参数 aVarOverrides 的值赋给成员变量 m_varOverrides。

    const std::string& GetType() const { return m_type; };
    bool IsCli() const { return m_isCli; };

    const std::map<wxString, wxString>& GetVarOverrides() const { return m_varOverrides; }

    void SetVarOverrides( const std::map<wxString, wxString>& aVarOverrides )
    {
        m_varOverrides = aVarOverrides;
    }

protected:
    // m_type: 一个字符串类型的成员变量，用于存储作业的类型。
    // m_isCli: 一个布尔类型的成员变量，用于表示对象是否在命令行环境下使用。
    // m_varOverrides: 一个 std::map<wxString, wxString> 类型的成员变量，用于存储作业的变量重写。
    std::string m_type;
    bool m_isCli;
    std::map<wxString, wxString> m_varOverrides;
};

#endif
