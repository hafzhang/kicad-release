#! /bin/bash

PROJECT_ROOT=`git rev-parse --show-toplevel`

BUILD_ROOT="$PROJECT_ROOT/build"

BIN_DIR="$BUILD_ROOT/out/bin/"

PYTHON_EXE="$BIN_DIR/python"

VCPKG_INSTALL_ROOT="$BUILD_ROOT/vcpkg_installed"

cmake --install $BUILD_ROOT

for i in  `find . -iname "*.pdb" |grep -v vcpkg_installed | grep -v build/out/bin ` ;do cp $i $BIN_DIR  ; done

find $VCPKG_INSTALL_ROOT -iname "*.dll" -exec cp '{}' $BIN_DIR \;


cp -rf  $VCPKG_INSTALL_ROOT/x64-windows/tools/python3/* $BIN_DIR

$PYTHON_EXE -m pip install -r "$PROJECT_ROOT/dev_utils/wxrequirements.txt"


