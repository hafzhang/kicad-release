/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2021 Jon Evans <jon@craftyjon.com>
 * Copyright (C) 2021-2022 KiCad Developers, see AUTHORS.txt for contributors.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <thread>

#include <core/wx_stl_compat.h>
#include <symbol_async_loader.h>
#include <symbol_lib_table.h>
#include <progress_reporter.h>

// 用于初始化各个成员变量。参数包括要加载的符号库的昵称（ aNicknames ）、符号库表（ aTable ）、
// 是否只加载电源符号（ aOnlyPowerSymbols ）、输出结果的容器指针（ aOutput ）以及
// 进度报告器（ aReporter ）。在构造函数中，初始化了成员变量并设置了线程数量。
SYMBOL_ASYNC_LOADER::SYMBOL_ASYNC_LOADER( const std::vector<wxString>& aNicknames,
        SYMBOL_LIB_TABLE* aTable, bool aOnlyPowerSymbols,
        std::unordered_map<wxString, std::vector<LIB_SYMBOL*>>* aOutput,
        PROGRESS_REPORTER* aReporter ) :
        m_nicknames( aNicknames ),
        m_table( aTable ),
        m_onlyPowerSymbols( aOnlyPowerSymbols ),
        m_output( aOutput ),
        m_reporter( aReporter ),
        m_nextLibrary( 0 )
{
    wxASSERT( m_table );
    // std::thread::hardware_concurrency() 返回计算机支持的并发线程数，std::max<size_t>(1, ...) 确保至少使用一个线程。
    m_threadCount = std::max<size_t>( 1, std::thread::hardware_concurrency() );

    m_returns.resize( m_threadCount );
}



SYMBOL_ASYNC_LOADER::~SYMBOL_ASYNC_LOADER()
{
    Join();
}

// 启动异步加载任务。它会遍历线程数（m_threadCount），为每个线程调用 worker 函数进行符号库加载。
void SYMBOL_ASYNC_LOADER::Start()
{
    for( size_t ii = 0; ii < m_threadCount; ++ii )
        // 通过 std::async 函数创建了一个异步任务。std::async 函数接受一个启动策略（ std::launch::async ）、
        // 一个可调用对象（ &SYMBOL_ASYNC_LOADER::worker 函数的指针）、及该可调用对象的参数（ this，即当前 SYMBOL_ASYNC_LOADER 对象的指针）。
        m_returns[ii] = std::async( std::launch::async, &SYMBOL_ASYNC_LOADER::worker, this );
}

// 遍历所有的异步任务，等待任务完成并处理加载结果。
bool SYMBOL_ASYNC_LOADER::Join()
{
    // for 循环遍历了所有的异步任务，等待它们完成。
    for( size_t ii = 0; ii < m_threadCount; ++ii )
    {
        // 对于每个异步任务，首先检查其是否有效（!m_returns[ii].valid()）。
        if( !m_returns[ii].valid() )
            continue;

        m_returns[ii].wait();
        // m_returns[ii].get() 获取异步任务的返回值,是一个包含加载结果的 std::vector<LOADED_PAIR>。
        const std::vector<LOADED_PAIR>& ret = m_returns[ii].get();
        // 如果输出容器不为空且加载结果也不为空，则遍历加载结果，并根据条件将其插入到输出容器中
        if( m_output && !ret.empty() )
        {
            for( const LOADED_PAIR& pair : ret )
            {
                // Don't show libraries that had no power symbols
                if( m_onlyPowerSymbols && pair.second.empty() )
                    continue;

                // *Do* show empty libraries in the normal case
                m_output->insert( pair );
            }
        }
    }

    return true;
}

// 用于检查是否所有的符号库都已加载完成。
bool SYMBOL_ASYNC_LOADER::Done()
{
    return m_nextLibrary.load() >= m_nicknames.size();
}

// 从符号库昵称列表中取出一个昵称，然后加载对应的符号库到输出容器中。
// 如果加载过程中出现异常，会捕获异常并记录错误信息。
std::vector<SYMBOL_ASYNC_LOADER::LOADED_PAIR> SYMBOL_ASYNC_LOADER::worker()
{
    // 用于存储加载结果的容器
    std::vector<LOADED_PAIR> ret;
    // 用于存储加载结果的容器
    bool onlyPower = m_onlyPowerSymbols;
    
    // 用于存储加载结果的容器
    for( size_t libraryIndex = m_nextLibrary++; libraryIndex < m_nicknames.size();
         libraryIndex = m_nextLibrary++ )
    {
        // 获取当前符号库昵称
        const wxString& nickname = m_nicknames[libraryIndex];

        // 如果有进度报告器，则更新加载进度
        if( m_reporter )
            m_reporter->AdvancePhase( wxString::Format( _( "Loading library %s..." ), nickname ) );

        // 如果进度报告器被取消，则退出加载循环
        if( m_reporter && m_reporter->IsCancelled() )
            break;

// 创建一个加载结果对象
        LOADED_PAIR pair( nickname, {} );

        try
        {
            m_table->LoadSymbolLib( pair.second, nickname, onlyPower );
            ret.emplace_back( std::move( pair ) );
        }
        catch( const IO_ERROR& ioe )
        {
            wxString msg = wxString::Format( _( "Error loading symbol library %s.\n\n%s\n" ),
                                             nickname, ioe.What() );

            // 在多线程环境中使用互斥锁保护错误信息的添加操作
            std::lock_guard<std::mutex> lock( m_errorMutex );
            m_errors += msg;
        }
        catch( std::exception& e )
        {
            wxString msg = wxString::Format( _( "Error loading symbol library %s.\n\n%s\n" ),
                                             nickname, e.what() );

            std::lock_guard<std::mutex> lock( m_errorMutex );
            m_errors += msg;
        }
    }

    return ret;
}
