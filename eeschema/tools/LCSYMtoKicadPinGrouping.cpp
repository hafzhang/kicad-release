﻿#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "LCSYMtoKicadPinGrouping.h"
#include "nlohmann_json/nlohmann/json.hpp"
#include <wx/arrstr.h>
//#include <symbol_library.h>
#include <lib_symbol.h>
#include <wx/string.h>


#include <pin_type.h>
#include<lib_pin.h>
#include <lib_shape.h>
#include <default_values.h>
#include<lib_field.h>
#include <template_fieldnames.h>
#include <lib_text.h>



#include <wx/log.h>
#include <dsnlexer.h>
#include <eda_shape.h>
#include <memory>
#include "LCSYMSort.h"
#include <regex>

using namespace std;
using nlohmann::json;

//立创专业版EDAsymbol文件数据扩大倍数
#define LCSYMExpendNum 100000 * 0.0254
//#define LCSYMLengthExpend 10 * 0.2

/**
 * Simple container to manage fill parameters.
 */
class FILL_PARAMS
{
public:
    FILL_T  m_FillType;
    COLOR4D m_Color;
};


LCSYMtoKicadPinGrouping::LCSYMtoKicadPinGrouping()
{
    m_symbolObj = nullptr;
    
}
LCSYMtoKicadPinGrouping ::~LCSYMtoKicadPinGrouping()
{
    // delete m_symbolObj;
}


//解析每一行symbol数据,meiyihang shiyige symbol
int LCSYMtoKicadPinGrouping::parseLines(  std::string vecLCSYMLines )
{

    string symbolName;
    wxString symbolReference;
    unordered_multimap<string,string> nuNaArray;
    vector<string> pinGrouping;
    string strLine = vecLCSYMLines;
    // //筛除无效数据

    json j = json::parse(strLine);
    std::cout << "Symbol: " << j["symbol"] << std::endl;
    symbolName =  j["symbol"];
    // strSYMFileName = symbolName;

    string fileName = j["fileName"];
    strSYMFileName = wxString( fileName.c_str(), wxConvUTF8 );

    string reference =  j["reference"];
    symbolReference = wxString( reference.c_str(), wxConvUTF8 );

    std::cout << "Number-Name Mapping:" << std::endl;
    for (const auto& item : j["number-name"]) {
        nuNaArray.insert(std::make_pair( item["key"] , item["value"] ));
        std::cout << "Key: " << item["key"] << ", Value: " << item["value"] << std::endl;
    }
    bool is = j.contains("out");
    std::cout << "out: " <<j["out"] << std::endl;
    auto output  = j["out"];
    std::string outputPin  =  j["out"];


    // std::regex pattern("\\[(.*?)\\]");
    std::regex pattern("([^|]+)"); // 匹配|字符之间的分组

    // 使用 std::sregex_iterator 迭代器获取所有匹配项
    std::sregex_iterator it(outputPin.begin(), outputPin.end(), pattern);
    std::sregex_iterator end;
    
    while (it != end) {
        // 获取第一个捕获组的内容，即 [] 中的内容
        std::string content = (*it)[1].str();
        int GroupingSize =  pinGrouping.size();
        // 使用 std::istringstream 处理逗号分隔的内容
        std::istringstream groupStream(content);
        std::string item;
        while (std::getline(groupStream, item, ',')) {
            // 在添加之前检查 item 是否已经存在于 pinGrouping 中
            if (std::find(pinGrouping.begin(), pinGrouping.end(), item) == pinGrouping.end()) {
                pinGrouping.push_back(item); // 如果不存在，添加到向量中
            }
        }

        // 添加 '#' 分隔符，除非是最后一个 [], 排除空的[]
        if (++it != end && GroupingSize != pinGrouping.size()) {
            pinGrouping.push_back("#");
        }
    }
    
    // for (const auto& elem : pinGrouping) {
    //     std::cout << elem << " ";
    // }

    VECTOR2I startPoint{0, 0};
    strSYMName = wxString( symbolName.c_str(), wxConvUTF8 );
    m_symbolObj = new LIB_SYMBOL( strSYMFileName );

    parseAllPIN( pinGrouping,symbolReference, nuNaArray, 1, 1 );

    return 0;
}

std::string LCSYMtoKicadPinGrouping::trim(const std::string& str) {
    size_t first = str.find_first_not_of(' ');
    if (std::string::npos == first) {
        return str;
    }
    size_t last = str.find_last_not_of(' ');
    return str.substr(first, (last - first + 1));
}

int LCSYMtoKicadPinGrouping::parseAllPIN(  std::vector<std::string>& allPinOfVecStr, wxString& wxReference,
std::unordered_multimap<std::string, std::string>& nuNaArray,  int m_unit,
                                    int m_convert )
{

    std::vector<std::vector<std::string>> symbolAllSide;
    std::vector<std::string> firstSide;
    std::vector<std::string> SecondSide;


    if (allPinOfVecStr.empty()) return 0;
    symbolAllSide = completeALLPin( allPinOfVecStr, nuNaArray );

    int SideCount = symbolAllSide.size();
    if ( SideCount == 2 ){
        firstSide = symbolAllSide[0];
        SecondSide = symbolAllSide[1];

    }else if ( SideCount == 1 ){
        firstSide = symbolAllSide[0];
    }
    else{
        return 0;
    }
    

//-------------------------计算字体的最大长度，确定边框位置---------------------------//
    size_t maxLengthLeft = 0;
    //比较leftPinNames每个字符串的长度
    auto leftCountEnd = firstSide.begin();
    for (auto it = firstSide.begin(); it != firstSide.end() ; ++it) {
        std::string name = *it;
        if (name.length() > maxLengthLeft) {
            maxLengthLeft = name.length();
        }
    }

    //比较 rightPinNames 每个字符串的长度
    size_t maxLengthRight = 0;
    for (auto it = SecondSide.begin(); it != SecondSide.end(); ++it) {
        std::string name = *it;
        if (name.length() > maxLengthRight) {
            maxLengthRight = name.length();
        }
    }

//-------------------------绘制方框-------------------------------//

    size_t RectangleWidth;
    VECTOR2I upperRight, leftLower, namePosition;
    int MaxSidePinCount = max( firstSide.size(), SecondSide.size() );
    int MaxSideModdlePinCount ;

    RectangleWidth = 10 * (( maxLengthLeft + maxLengthRight + 8 )/2);
    upperRight.x = RectangleWidth / 2;


    if(MaxSidePinCount % 2 == 0 )  MaxSideModdlePinCount = ( MaxSidePinCount/2 );
    else  MaxSideModdlePinCount = ( MaxSidePinCount/2 + 1 );

    upperRight.y = ( (MaxSideModdlePinCount - 1 ) * 10 + 5 );
    leftLower.x = - RectangleWidth / 2;
    leftLower.y = - (( MaxSidePinCount - MaxSideModdlePinCount ) *10 + 5 );
    
    int def = drawingRECT(upperRight, leftLower, m_unit, m_convert );

    namePosition.x = 0;
    namePosition.y = LCSYMExpendNum * upperRight.y;
    parsePART( strSYMName,wxReference,namePosition, 1, 1);

//-----------------绘制引脚--------------------------------//
    std::vector<std::string> pinAllInfo;
    int j = 0;
    int processedCount = 0;
    
    for (auto it = firstSide.begin(); it != firstSide.end() ; ++it) {
        if( *it=="#" ){
            j++;
            continue;
        }
        int x_pin = -(RectangleWidth / 2 + 15);
        int y_pin = ( (MaxSideModdlePinCount - 1 - (j++) ) * 10 );

        // [0]:x，[1]:y,定义引脚坐标
        pinAllInfo.push_back(std::to_string(x_pin));
        pinAllInfo.push_back(std::to_string(y_pin));
        // [2],定义方向
        pinAllInfo.insert(pinAllInfo.begin() + 2, "0");
        string pinNumber;
        // 遍历 multimap 并找到匹配的键 
        // auto item = nuNaArray.end();
        // for (const auto& pair : nuNaArray) {
        //     if (pair.second == *it) {
        //         pinNumber = pair.first;
        //         item = nuNaArray.find(pair.first); // 记录找到的迭代器
        //         break;
        //     }
        // }
        auto item = nuNaArray.end();
        for (const auto& pair : nuNaArray) {
            if (pair.first == *it) {
                pinNumber = pair.second;
                item = nuNaArray.find(pair.first); // 记录找到的迭代器
                break;
            }
        }
        // 如果找到了匹配项，删除这个元素
        if (item != nuNaArray.end()) {
            nuNaArray.erase(item);
        }
        // [3],定义引脚number
        pinAllInfo.insert(pinAllInfo.begin() + 3, pinNumber );
        // [4],定义引脚名称
        pinAllInfo.insert(pinAllInfo.begin() + 4, *it );

        parsePIN( pinAllInfo, m_unit, m_convert );
        pinAllInfo.clear();
    }

    j = 0;
    // 逆向遍历
    // for (auto rIt = SecondSide.rbegin(); rIt != SecondSide.rend() ; ++rIt) 
    for (auto rIt = SecondSide.begin(); rIt != SecondSide.end() ; ++rIt) 
    {
        if( *rIt=="#" ){
            j++;
            continue;
        }
        int x_pin = RectangleWidth / 2 + 15;
        int y_pin = ( (MaxSideModdlePinCount - 1 - (j++) ) * 10 );
        // [0]:x，[1]:y,定义引脚坐标
        pinAllInfo.push_back(std::to_string(x_pin));
        pinAllInfo.push_back(std::to_string(y_pin));
        // [2],定义方向
        pinAllInfo.insert(pinAllInfo.begin() + 2, "180" );
        string pinNumber;
        // 遍历 multimap 并找到匹配的键
        auto item = nuNaArray.end();

        for (const auto& pair : nuNaArray) {
            if (pair.first == *rIt) {
                pinNumber = pair.second;
                item = nuNaArray.find(pair.first); // 记录找到的迭代器
                break;
            }
        }
        // 如果找到了匹配项，删除这个元素
        if (item != nuNaArray.end()) {
            nuNaArray.erase(item);
        }
        
        // [3],定义引脚number
        // allPinOfVecStr.size()/2
        pinAllInfo.insert(pinAllInfo.begin() + 3, pinNumber );

        // [4],定义引脚名称
        pinAllInfo.insert(pinAllInfo.begin() + 4, *rIt );
    
        parsePIN( pinAllInfo, m_unit, m_convert );
        pinAllInfo.clear();
    }

    return 0;
}




std::vector<std::vector<std::string>> LCSYMtoKicadPinGrouping::completeALLPin(  std::vector<std::string>& allPinOfVecStr, 
std::unordered_multimap<std::string, std::string>& nuNaArray )
{

    std::vector<std::vector<std::string>> allsidePins;

    std::vector<std::string> sourcesToCheck = {"VCC", "VDD", "+5V", "+12V", "5V", "12V", "VD", "VPP", "VCCQ","GND", "VSS", "VSSA", "AGND", "DGND", "PGND", "GROUND" };
    // 使用erase-remove idiom来删除向量中的元素
    for (auto it = allPinOfVecStr.begin(); it != allPinOfVecStr.end(); ) {
        if (std::find(sourcesToCheck.begin(), sourcesToCheck.end(), *it) != sourcesToCheck.end()) {
            it = allPinOfVecStr.erase(it); // 从向量中删除元素，并继续从下一个元素开始迭代
        } else {
            ++it; // 没有找到，继续迭代
        }
    }



    // 遍历 allPinOfVecStr，查找对应键的值并插入到 resultVector 的指定位置
    for (size_t i = 0; i < allPinOfVecStr.size(); ) {
        if( allPinOfVecStr[i] == "#") {
            ++i;
            continue;
        }
        auto it = nuNaArray.find(allPinOfVecStr[i]);
        if (it == nuNaArray.end()) {
        // 如果元素不存在，删除它
            allPinOfVecStr.erase(allPinOfVecStr.begin() + i);
        } else{
            auto range = nuNaArray.equal_range(allPinOfVecStr[i]);
            size_t count = std::distance(range.first, range.second);
            
            if (count > 1) {
                size_t insertPosition = i+1;
                std::vector<std::string> valuesToInsert;
                auto it = range.first;
                ++it; // 从第二个元素开始
                while (it != range.second) {
                    valuesToInsert.push_back(it->first);
                    ++it;
                }
                // 在指定位置插入收集到的值
                allPinOfVecStr.insert(allPinOfVecStr.begin() + insertPosition, valuesToInsert.begin(), valuesToInsert.end());

                // 更新插入位置，假设我们希望在插入的所有值之后继续插入
                i += valuesToInsert.size();
            }
            ++i;
        }
    }


    // 分组并计数
    std::vector<std::vector<std::string>> groups;
    std::vector<int> groupSizes;
    std::vector<std::string> currentGroup;
    size_t i = 0;
 
    while (i < allPinOfVecStr.size()) {
        if (allPinOfVecStr[i] == "#") {
            if (!currentGroup.empty()) {
                groups.push_back(currentGroup);
                currentGroup.clear();
            }
            ++i;
        } else {
            currentGroup.push_back(allPinOfVecStr[i]);
            ++i;
        }
    }
    // Add the last group if it's not empty
    if (!currentGroup.empty()) {
        groups.push_back(currentGroup);
    }

    // 根据组的大小进行降序排序
    std::sort(groups.begin(), groups.end(), [](const std::vector<std::string>& a, const std::vector<std::string>& b) {
        return a.size() > b.size();
    });

    // 重新组合向量
    allPinOfVecStr.clear();
    for (const auto& group : groups) {
        allPinOfVecStr.insert(allPinOfVecStr.end(), group.begin(), group.end());
        allPinOfVecStr.push_back("#");
    }
    // 移除最后的"#"，如果需要的话
    if (!allPinOfVecStr.empty() && allPinOfVecStr.back() == "#") {
        allPinOfVecStr.pop_back();
    }




    std::vector<std::string> voltageSources = {"VCC", "VDD", "+5V", "+12V", "5V", "12V", "VD", "VPP", "VCCQ"};
    std::vector<std::string> groundSources ={"GND", "VSS", "VSSA", "AGND", "DGND", "PGND", "GROUND" };

    std::vector<std::string> voltageKeys;
    for (const auto& voltageSource : voltageSources) {
        auto range = nuNaArray.equal_range(voltageSource);
        for (auto it = range.first; it != range.second; ++it) {
            voltageKeys.push_back(it->first);
        }
    }
    
    std::vector<std::string> groundKeys;
    for (const auto& ground : groundSources) {
        auto range = nuNaArray.equal_range(ground);
        for (auto it = range.first; it != range.second; ++it) {
            groundKeys.push_back(it->first);
        }
    }
    int offset_value = 0;
    if( !groundKeys.empty() || !voltageKeys.empty()){
        offset_value = (groundKeys.size() - voltageKeys.size())/2;
    }
    // 找到中间位置的元素
    size_t midIndex = allPinOfVecStr.size() / 2;
    
    midIndex = midIndex + offset_value;
   // 从中间位置开始向后查找第一个元素等于 "#". targetIndex是元素的个数
    size_t targetIndex = allPinOfVecStr.size();
    for (size_t i = midIndex ; i < allPinOfVecStr.size(); ++i) {
        if (allPinOfVecStr[i] == "#") {
            targetIndex = i;
            break;
        }
    }
    
    if (targetIndex != std::string::npos) {
        // 存储 "#" 之前的元素
        std::vector<std::string> beforeS(allPinOfVecStr.begin(), allPinOfVecStr.begin() + targetIndex);
        allsidePins.push_back(beforeS);

        // 存储 "#" 之后的所有元素
        if (targetIndex < allPinOfVecStr.size() ) {
            std::vector<std::string> afterS(allPinOfVecStr.begin() + targetIndex +1 , allPinOfVecStr.end());
            if( !afterS.empty() ){
                allsidePins.push_back(afterS);
            }
        }
    }



    int SideCount = allsidePins.size();
    if( !voltageKeys.empty()){
        allsidePins[0].push_back("#");
        allsidePins[0].insert(allsidePins[0].end(), voltageKeys.begin(), voltageKeys.end());
    }
    if( !groundKeys.empty() && SideCount > 1 ){
        allsidePins[1].push_back("#");
        allsidePins[1].insert(allsidePins[1].end(), groundKeys.begin(), groundKeys.end());
    }else if( !groundKeys.empty() && SideCount == 1 ){
        allsidePins.push_back(groundKeys);
    }
    
    // 收集 allsidePins 中的所有元素
    std::vector<std::string> allElements;
    for (const auto& vec : allsidePins) {
        allElements.insert(allElements.end(), vec.begin(), vec.end());
    }
    // Store missing pin
    std::vector<std::string> missingPinNames;
    // Ergodic nuNaArray's key
    for (auto it = nuNaArray.begin(); it != nuNaArray.end(); ++it) {
        const std::string& key = it->first;
        // 检查 key 是否在 allElements 中存在
        if (std::find(allElements.begin(), allElements.end(), key) == allElements.end()) {
            missingPinNames.push_back(key);
        }
    }
    if( !missingPinNames.empty() ){
        missingPinNames.push_back("#");
        if (allsidePins.size() == 1){
            allsidePins.push_back(missingPinNames);
        }else{
            allsidePins[1].insert(allsidePins[1].begin(), missingPinNames.begin(), missingPinNames.end());
        }

    }
    return allsidePins;

}


//解析PART
int LCSYMtoKicadPinGrouping::parsePART( wxString& wxName,wxString& wxReference,
                                    VECTOR2I& f_xy, int m_unit, int m_convert )
{
    int resultID =0;

    // wxName = wxString( SymbolName.c_str(), wxConvUTF8 );

    // wxType = wxString( vecStr[4].c_str(), wxConvUTF8 );

    m_fieldIDsRead.clear();
    // Make sure the mandatory field IDs are reserved as already read,
    // the field parser will set the field IDs to the correct value if
    // the field name matches a mandatory field name
    for( int i = 0; i < MANDATORY_FIELDS; i++ )
        m_fieldIDsRead.insert( i );
    

    LIB_ID id( wxName, wxName );
    m_symbolObj->SetName( wxName );
    m_symbolObj->SetLibId( id );

    m_symbolObj->SetPinNameOffset( LCSYMExpendNum * 3.993 );
    m_symbolObj->SetShowPinNames( true );
    m_symbolObj->SetShowPinNumbers( true );
    m_symbolObj->SetExcludedFromBOM( false );
    m_symbolObj->SetExcludedFromBoard( false );

    std::map<wxString, wxString> myProperty;
    myProperty.insert( std::make_pair( "Reference", wxReference ) );
    myProperty.insert( std::make_pair( "Value", wxName ) );
    myProperty.insert( std::make_pair( "Footprint", "" ) );
    myProperty.insert( std::make_pair( "Datasheet", "" ) );
    myProperty.insert( std::make_pair( "Description", "" ) );

    for( std::map<wxString, wxString>::iterator it = myProperty.begin(); it != myProperty.end();
         ++it )
    {
        m_name = it->first;
        m_value = it->second;

        // wxString name = it->first;
        // wxString value = it->second;
        setPROPERTY( m_name, m_value, f_xy );
    }

    return resultID;
}


//设置kicad的property
LIB_FIELD* LCSYMtoKicadPinGrouping::setPROPERTY( const wxString& name, const wxString& value,
                                         VECTOR2I& f_xy )
{
    //设置属性
    LIB_FIELD* field = new LIB_FIELD( m_symbolObj, MANDATORY_FIELDS );
    // std::unique_ptr<LIB_FIELD> field = std::make_unique<LIB_FIELD>(m_symbolObj.get(), MANDATORY_FIELDS);
    fp_xy = f_xy;

    if( name == "Reference" )
    {
        //field->SetName( name );
        for( int ii = 0; ii < MANDATORY_FIELDS; ++ii )
        {
            if( !name.CmpNoCase( TEMPLATE_FIELDNAME::GetDefaultFieldName( ii ) ) )
            {
                field->SetId( ii );
                break;
            }
        }
        field->SetText( value );
        fp_xy.y = ( fp_xy.y + LCSYMExpendNum * stof( "12.7" ) );
    }
    else if( name == "Value" )
    {
        //field->SetName( name );
        for( int ii = 0; ii < MANDATORY_FIELDS; ++ii )
        {
            if( !name.CmpNoCase( TEMPLATE_FIELDNAME::GetDefaultFieldName( ii ) ) )
            {
                field->SetId( ii );
                break;
            }
        }
        field->SetText( value );
        fp_xy.y = ( fp_xy.y + LCSYMExpendNum * stof( "5.7" ) );
    }
    else
    {
        if( !field->IsMandatory() )
        {
            // field->SetName( name );
            // Correctly set the ID based on canonical (untranslated) field name
            // If ID is stored in the file (old versions), it will overwrite this
            for( int ii = 0; ii < MANDATORY_FIELDS; ++ii )
            {
                if( !name.CmpNoCase( TEMPLATE_FIELDNAME::GetDefaultFieldName( ii ) ) )
                {
                    field->SetId( ii );
                    break;
                }
            }
            field->SetText( value );
        }
    }
    //parsePROPERTY_TEXT( static_cast<EDA_TEXT*>( field.get() ) );
    
    field->SetPosition( fp_xy );
    field->SetTextAngle( EDA_ANGLE( 0.0, DEGREES_T ) );
    field->SetNameShown( false );
    field->SetCanAutoplace( true );

    LIB_FIELD* existingField;
    existingField = m_symbolObj->GetFieldById( field->GetId() );
    *existingField = *field;
    m_fieldIDsRead.insert( field->GetId() );
    return existingField;

    if( !existingField )
    {
        m_symbolObj->AddDrawItem( field, false );
        m_fieldIDsRead.insert( field->GetId() );
    }
    else
    {
        // We cannot handle 2 fields with the same name, so skip this one
        return nullptr;
    }
}








//修改kicad的property
void LCSYMtoKicadPinGrouping::changePROPERTY( LIB_SYMBOL* curSymbol )
{
    LIB_FIELD* field_val = curSymbol->GetFieldById( 1 );
    field_val->SetText( wxName );
    //fp_xy.y = ( fp_xy.y + LCSYMExpendNum * stof( "5.7" ) );
    field_val->SetPosition( fp_xy );
    LIB_FIELD* field_ref = curSymbol->GetFieldById( 0 );
    field_ref->SetText( "" );
    fp_xy.y = ( fp_xy.y + LCSYMExpendNum * stof( "7" ) );
    field_ref->SetPosition( fp_xy );
}


    //解析RECT(矩形)
int LCSYMtoKicadPinGrouping::drawingRECT(const VECTOR2I& upperRight,const VECTOR2I& leftLower, int m_unit, int m_convert )
{
    int resultID = 0;
    LIB_SHAPE* rectangle = new LIB_SHAPE(m_symbolObj, SHAPE_T::RECTANGLE);
    VECTOR2I startPoint( 0, 0 ); // 左下角坐标
    VECTOR2I endPoint( 1, 1 );   // 右上角坐标
    STROKE_PARAMS stroke( schIUScale.MilsToIU(  DEFAULT_LINE_WIDTH_MILS/0.6 ),LINE_STYLE::DEFAULT );
    FILL_PARAMS   fill;
    


    rectangle->SetUnit( m_unit );
    rectangle->SetBodyStyle( m_convert );
    rectangle->SetPrivate( false );


    startPoint.x = LCSYMExpendNum * leftLower.x;
    startPoint.y = LCSYMExpendNum * leftLower.y;
    endPoint.x = LCSYMExpendNum * upperRight.x;
    endPoint.y = LCSYMExpendNum * upperRight.y;

    rectangle->SetPosition( startPoint );
    rectangle->SetEnd( endPoint );
    // rectangle->SetPosition( leftLower );
    // rectangle->SetEnd( upperRight );
    rectangle->SetStroke( stroke );
    rectangle->SetFillMode( fill.m_FillType );
    rectangle->SetFillColor( fill.m_Color );

    rectangle->SetFillMode( FILL_T::FILLED_WITH_BG_BODYCOLOR );
    rectangle->SetFillColor( lightYellow );

    m_symbolObj->AddDrawItem( rectangle, false );
    // delete rectangle;
    return resultID;
}

//解析PIN
int LCSYMtoKicadPinGrouping::parsePIN( std::vector<std::string> vecStr, int m_unit,
                                    int m_convert )
{
    int resultID = 0;
    LIB_PIN*  pin = new LIB_PIN( m_symbolObj );
    // std::unique_ptr<LIB_PIN> pin(new LIB_PIN(m_symbolObj.get()));
    COLOR4D  color = COLOR4D::UNSPECIFIED;
    wxString  wxPinName;
    wxString  wxPinNumber;

    pin->SetUnit( m_unit );
    pin->SetBodyStyle( m_convert );
    
    // Pin shape 没有对应，设置默认
    pin->LIB_PIN::SetShape( GRAPHIC_PINSHAPE::LINE );
    pin->SetVisible(true );

    //定义、设置引脚坐标
    VECTOR2I p_xy( 0, 0 );
    p_xy.x = LCSYMExpendNum * stof( vecStr[0] );
    p_xy.y = LCSYMExpendNum * stof( vecStr[1] );
    pin->SetPosition( p_xy );

    //Pin length
    int p_length = LCSYMExpendNum * 15;
    pin->SetLength( p_length );


    //Pin orientation of rotation
    int p_orientation = stoi( vecStr[2] );
    if( p_orientation == 0 )
    {
        pin->SetOrientation( PIN_ORIENTATION::PIN_RIGHT );
    }
    else if( p_orientation == 90 )
    {
        pin->SetOrientation( PIN_ORIENTATION::PIN_UP );
    }
    else if( p_orientation == 180 )
    {
        pin->SetOrientation( PIN_ORIENTATION::PIN_LEFT );
    }
    else if( p_orientation == 270 )
    {
        pin->SetOrientation( PIN_ORIENTATION::PIN_DOWN );
    }

    wxPinNumber = wxString( vecStr[3].c_str(), wxConvUTF8 );
    //pin->SetNumber( wxPinNumber );
    wxPinName = wxString( vecStr[4].c_str(), wxConvUTF8 );
    //pin->SetName( wxPinName );


    // ELECTRICAL_PINTYPE pinType = ELECTRICAL_PINTYPE::PT_PASSIVE ;

    ELECTRICAL_PINTYPE pinType = ELECTRICAL_PINTYPE::PT_PASSIVE;
    pin->SetType( pinType );
    
    pin->SetName( wxPinName );
    pin->SetNumber( wxPinNumber );

    m_symbolObj->AddDrawItem( pin, false );

    // delete pin;
    return resultID;
}


//解析ARC(弧线)
int LCSYMtoKicadPinGrouping::parseARC( std::vector<std::string> vecStr, int m_unit, int m_convert )
{
    int resultID = 0;

    LIB_SHAPE* arc = new LIB_SHAPE( m_symbolObj, SHAPE_T::ARC );
    VECTOR2I   startPoint( 1, 0 ); // Initialize to a  arc just for safety
    VECTOR2I   midPoint( 1, 1 );
    VECTOR2I   endPoint( 0, 1 );
    

    //设置默认stroke
    STROKE_PARAMS stroke( schIUScale.MilsToIU( DEFAULT_LINE_WIDTH_MILS ), LINE_STYLE::DEFAULT );
    //设置默认fill;
    FILL_PARAMS fill;
    fill.m_FillType = FILL_T::NO_FILL;
    fill.m_Color = COLOR4D::UNSPECIFIED;

    arc->SetUnit( m_unit );
    arc->SetBodyStyle( m_convert );
    //private单元仅用于组成一个复杂符号,不会单独使用。
    arc->SetPrivate( false );

    startPoint.x = LCSYMExpendNum * stof( vecStr[2] );
    startPoint.y = LCSYMExpendNum * stof( vecStr[3] );
    midPoint.x = LCSYMExpendNum * stof( vecStr[4] );
    midPoint.y = LCSYMExpendNum * stof( vecStr[5] );
    endPoint.x = LCSYMExpendNum * stof( vecStr[6] );
    endPoint.y = LCSYMExpendNum * stof( vecStr[7] );

    arc->SetArcGeometry( startPoint, midPoint, endPoint );
    arc->SetStroke( stroke );
    arc->SetFillMode( fill.m_FillType );
    arc->SetFillColor( fill.m_Color );

    m_symbolObj->AddDrawItem( arc, false );
    return resultID;
}


    //解析RECT(矩形)
VECTOR2I LCSYMtoKicadPinGrouping::parseRECT( std::vector<std::string> vecStr, int m_unit, int m_convert )
{

    VECTOR2I startPoint( 0, 0 ); // 左下角坐标
    VECTOR2I endPoint( 1, 1 );   // 右上角坐标


    startPoint.x = LCSYMExpendNum * stof( vecStr[2] );
    startPoint.y = LCSYMExpendNum * stof( vecStr[3] );
    endPoint.x = LCSYMExpendNum * stof( vecStr[4] );
    endPoint.y = LCSYMExpendNum * stof( vecStr[5] );

    return endPoint;
}

//解析CIRCLE(圆形)
int LCSYMtoKicadPinGrouping::parseCIRCLE( std::vector<std::string> vecStr, int m_unit, int m_convert )
{

    LIB_SHAPE* circle = new LIB_SHAPE( m_symbolObj, SHAPE_T::CIRCLE );
    VECTOR2I   center( 0, 0 );
    int radius = 1; // defaulting to 0 could result in troublesome math....
    STROKE_PARAMS stroke( schIUScale.MilsToIU( DEFAULT_LINE_WIDTH_MILS ), LINE_STYLE::DEFAULT );
    FILL_PARAMS   fill;

    circle->SetUnit( m_unit );
    circle->SetBodyStyle( m_convert );
    circle->SetPrivate( false );

    center.x = LCSYMExpendNum * stof( vecStr[2] );
    center.y = LCSYMExpendNum * stof( vecStr[3] );
    radius = LCSYMExpendNum * stof( vecStr[4] );

    circle->SetStroke( stroke );
    circle->SetFillMode( fill.m_FillType );
    circle->SetFillColor( fill.m_Color );
    circle->SetCenter( center );
    circle->SetEnd( VECTOR2I( center.x + radius, center.y ) );

    m_symbolObj->AddDrawItem( circle, false );
    return 0;
}


    //解析POLY(多边形)
int LCSYMtoKicadPinGrouping::parsePOLY( std::vector<std::string> vecStr, int m_unit, int m_convert )
{
    int resultID = 0;
    int numPointsToAdd = 0;
    LIB_SHAPE*    poly = new LIB_SHAPE( m_symbolObj, SHAPE_T::POLY );
    STROKE_PARAMS stroke( schIUScale.MilsToIU( DEFAULT_LINE_WIDTH_MILS ), LINE_STYLE::DEFAULT );
    FILL_PARAMS   fill;


    poly->SetUnit( m_unit );
    poly->SetBodyStyle( m_convert );
    poly->SetPrivate( false );


    for( int i = 0; i < vecStr.size(); i++ )
    {
        if( vecStr[i].find( "st" ) != std::string::npos )
        {
            numPointsToAdd = ( i - 7 ) / 2 + 2;

            for( int j = 0; j < numPointsToAdd; j++ )
            {
                VECTOR2I point;
                point.x = LCSYMExpendNum * stof( vecStr[2 + j * 2] );
                point.y = LCSYMExpendNum * stof( vecStr[3 + j * 2] );
                poly->AddPoint( point );
            }
        }
    }
    poly->SetStroke( stroke );
    poly->SetFillMode( fill.m_FillType );
    poly->SetFillColor( fill.m_Color );

    m_symbolObj->AddDrawItem( poly, false );
    return resultID;
}


//解析TEXT
int LCSYMtoKicadPinGrouping::parseTEXT( std::vector<std::string> vecStr, int m_unit, int m_convert )
{
    int       resultID = 0;

    LIB_TEXT* text = new LIB_TEXT( m_symbolObj );
    VECTOR2I  pointText( 0, 0 );
    double  angle = 0.0;

    text->SetUnit( m_unit );
    text->SetBodyStyle( m_convert );
    text->SetPrivate( false );
    
    text->SetText( wxString( vecStr[5].c_str(), wxConvUTF8 ) );
    pointText.x = LCSYMExpendNum * stof( vecStr[2] );
    pointText.y = LCSYMExpendNum * stof( vecStr[3] );
    text->SetPosition( pointText );
    //angle
    //angle = stof( vecStr[4] );
    //text->SetTextAngle( EDA_ANGLE( angle, DEGREES_T ) );
    text->SetTextAngle( EDA_ANGLE( 0.0, DEGREES_T ) );
    
    m_symbolObj->AddDrawItem( text, false );
    return resultID;
}



//字符串分割
std::vector<std::string> LCSYMtoKicadPinGrouping::Split( std::string strContext,
                                                     std::string StrDelimiter )
{
    vector<string> vecResult;
    if( strContext.empty() )
    {
        return vecResult;
    }
    if( StrDelimiter.empty() )
    {
        vecResult.push_back( strContext );
        return vecResult;
    }

    strContext += StrDelimiter;
    int iSize = strContext.size();
    for( int i = 0; i < iSize; i++ )
    {
        int iPos = strContext.find( StrDelimiter, i );
        if( iPos < iSize )
        {
            string strElement = strContext.substr( i, iPos - i );
            vecResult.push_back( strElement );
            i = iPos + StrDelimiter.size() - 1;
        }
    }
    return vecResult;
}


//去除vector容器内单个元素的前后双引号
void LCSYMtoKicadPinGrouping::vecStrAnalyse( std::vector<std::string> vecStr )
{
    for( int i = 0; i < vecStr.size(); i++ )
    {
        if( vecStr[i].size() > vecStr[i].find( "\"" ) )
        {
            string str = vecStr[i].substr( 1, vecStr[i].size() - 2 );
            vecStr[i] = vecStr[i].substr( 1, vecStr[i].size() - 2 );
        }
    }
}

/// <summary>
/// 字符串批量替换
/// <param name="str">输入的文本</param>
/// <param name="a">目标文本</param>
/// <param name="b">替换内容</param>
/// <returns>替换好的文本</returns>
std::string LCSYMtoKicadPinGrouping::spp( std::string str, std::string a, std::string b )
{
    int oldPos = 0;
    while( str.find( a, oldPos ) != -1 ) //在未被替换的文本中寻找目标文本
    {
        int start = str.find( a, oldPos ); //找到目标文本的起始下标
        str.replace( start, a.size(), b );
        oldPos = start + b.size(); //记录未替换文本的起始下标
    }
    return str;
}


